#ifndef SYMBOL_TABLE
#define SYMBOL_TABLE

#include <fstream>
#include <list>
#include "type.hpp"

using namespace std;

#define VOID_ST 201
#define FLOAT_ST 202
#define INT_ST 203
#define AFLOAT_ST 204

/**
 symbol 
 */
class symbol {
public:

  enum struct kinds {
    subroutine_name = 101,
    subroutine_arg,
    local_var,
    aux_var,
    global_var,
  };

  string name;
  kinds kind;
  type_data type;
  /// initialization value
  string val;
  /// array dimension
  int dim;

  symbol() : dim(0) { }

/**
 unparse the symbol 
 */
  void unparse_decl(ostream&);
  void unparse_access(ostream&);

  static symbol constant(type_data type, string val) {
    symbol sym;
    sym.type = type;
    sym.val = val;
    return sym;
  };
};

/**
 symbol table
 */
class symbol_table {
public:
/**
 symbol table is stored as simple list of strings;
 */
  list<symbol*> tab;
  symbol_table() { }
  ~symbol_table() {
    list<symbol*>::iterator it;
    if (!tab.empty()) {
      for (it = tab.begin(); it != tab.end(); it++) delete *it;
    }
  }

  /// Adds a new symbol or returns an existing symbol
  symbol *insert(const string& name);

  /// Finds the symbol by name or returns null
  symbol *find(const string& name);

/**
 unparse the symbol table
 */
  void unparse_decl(ostream&, int);

};

#endif
