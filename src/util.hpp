#ifndef UTIL
#define UTIL

#define indent(out) do { for (int ii=0;ii<indent;ii++) out << "    "; } while (0)
#define child(v, n) (*std::next((v)->children.begin(), n))

#endif