#ifndef AST
#define AST

#include <fstream>
#include <vector>
#include <list>
#include "symbol_table.hpp"
#include "type.hpp"

enum astv_type {
  UNDEFINED_ASTV = 1000,
  SUBROUTINE_ASTV,
  SUBROUTINE_ARG_LIST_ASTV,
  SEQUENCE_OF_STATEMENTS_ASTV,
  IF_STATEMENT_ASTV,
  WHILE_STATEMENT_ASTV,
  ASSIGNMENT_ASTV,
  ADD_EXPRESSION_ASTV,
  MUL_EXPRESSION_ASTV,
  UPLUS_EXPRESSION_ASTV,
  UMINUS_EXPRESSION_ASTV,
  SIN_EXPRESSION_ASTV,
  EXP_EXPRESSION_ASTV,
  EVAL_EXPRESSION_ASTV,
  TRANSPOSE_EXPRESSION_ASTV,
  INVERSE_EXPRESSION_ASTV,
  TRACE_EXPRESSION_ASTV,
  DETERMINANT_EXPRESSION_ASTV,
  DOT_EXPRESSION_ASTV,
  SOLVE_EXPRESSION_ASTV,
  SOLVE_TRANSPOSED_EXPRESSION_ASTV,
  DECOMP_EXPRESSION_ASTV,
  DIM_ASTV,
  SYMBOL_ASTV,
  TYPE_ASTV,
  CONSTANT_ASTV,
  COS_EXPRESSION_ASTV,
  CONDITION_LT_ASTV,
  CONDITION_EQ_ASTV,
  SUB_EXPRESSION_ASTV,
  SCALAR_MEMREF_ASTV,
  PAR_EXPRESSION_ASTV,
  DIV_EXPRESSION_ASTV,
  SQRT_EXPRESSION_ASTV,
  ATAN_EXPRESSION_ASTV,
  CONDITION_NEQ_ASTV,
  CONDITION_GT_ASTV,
  CONDITION_GE_ASTV,
  CONDITION_LE_ASTV,
  ARRAY_MEMREF_ASTV,
  SUBROUTINE_CALL_ASTV,
  SUBROUTINE_CALL_ARG_LIST_ASTV,
  CODE_ASTV,
  SEQUENCE_OF_DECLARATIONS_ASTV,
  SEQUENCE_OF_SUBROUTINES_ASTV,
  POW_EXPRESSION_ASTV,
  LOG_EXPRESSION_ASTV,
  TAN_EXPRESSION_ASTV
};

#define UNDEF_ARRAY_IDX 10
#define SYMBOL_ARRAY_IDX 11
#define CONSTANT_ARRAY_IDX 12


using namespace std;

/**   
 ast vertex
 */
class ast_vertex {
public:
/**   
 unique index
 */
  int idx;
/**   
 ast vertex type
 */
  astv_type type;
/** 
 parent
 */
  ast_vertex* parent;
/** 
 children
 */
  list<ast_vertex*> children;

  ast_vertex(astv_type type) : type(type), parent(0) {
    static int i=0; idx=i++;
    children.clear();
  }
  virtual ~ast_vertex() {
    list<ast_vertex*>::iterator i;
    for (i=children.begin();i!=children.end();i++) delete *i;
  }

public:

/** 
 unparser
 */
  virtual void unparse(ostream&,int);

};

/**   
 abstract ast vertex for expressions
 */
class expr_ast_vertex : public ast_vertex {
public:
  symbol *sac_var;

  expr_ast_vertex(astv_type type) : ast_vertex(type) { }
  ~expr_ast_vertex() {}

  virtual type_data get_res_type(void) = 0;
};

/**
 * ast vertex for unary expressions
 */
class unary_expr_ast_vertex : public expr_ast_vertex {
public:
  
  type_data res_type;
  expr_ast_vertex *child;

  unary_expr_ast_vertex(astv_type type) : expr_ast_vertex(type), child(NULL) { }
  ~unary_expr_ast_vertex() {
    delete child;
  }

  type_data get_res_type(void) { return res_type; }

  virtual void unparse(ostream&,int);
};

/**
 * ast vertex for binary expressions
 */
class binary_expr_ast_vertex : public expr_ast_vertex {
public:
  
  type_data res_type;
  expr_ast_vertex *lhs;
  expr_ast_vertex *rhs;

  binary_expr_ast_vertex(astv_type type) : expr_ast_vertex(type), lhs(NULL), rhs(NULL) { }
  ~binary_expr_ast_vertex() {
    delete lhs;
    delete rhs;
  }

  type_data get_res_type(void) { return res_type; }

  virtual void unparse(ostream&,int);
};

/**   
 ast vertex for symbols, constants and memrefs
 */
class symbol_ast_vertex : public expr_ast_vertex {
public:
/** 
 pointer to symbol
 */
  symbol *sym;

  symbol_ast_vertex(astv_type type, symbol *sym) : expr_ast_vertex(type), sym(sym) { }
  symbol_ast_vertex(symbol *sym) : symbol_ast_vertex(SYMBOL_ASTV, sym) { }
  ~symbol_ast_vertex() {}

  type_data get_res_type(void) { return sym->type; }

  virtual void unparse(ostream&,int);
};

/**   
 ast vertex for constants
 */
class const_ast_vertex : public symbol_ast_vertex {
public:

  const_ast_vertex(symbol *sym) : symbol_ast_vertex(CONSTANT_ASTV, sym) { }
  ~const_ast_vertex() {}
};

/**   
 ast vertex for scalar variables
 */
class memref_ast_vertex : public symbol_ast_vertex {
public:

  memref_ast_vertex(symbol *sym) : symbol_ast_vertex(SCALAR_MEMREF_ASTV, sym) { }
  ~memref_ast_vertex() {}
};

/**   
 ast vertex for array variables
 */
class array_memref_ast_vertex : public memref_ast_vertex {
public:
/** 
 pointer to offset 
 */
  symbol *offset;

  array_memref_ast_vertex(symbol *sym) : memref_ast_vertex(sym) { 
    type=ARRAY_MEMREF_ASTV;
  }
  ~array_memref_ast_vertex() {}

  virtual void unparse(ostream&,int);
};

/**   
 ast vertex for types
 */
class type_ast_vertex : public ast_vertex {
public:
  type_data data;

  type_ast_vertex(type_data data) : ast_vertex(TYPE_ASTV), data(data) { }
  ~type_ast_vertex() {}

  virtual void unparse(ostream&,int);
};

/**
 ast vertex for assignments
 */
class assignment_ast_vertex : public ast_vertex {
public:

  memref_ast_vertex *lhs;
  expr_ast_vertex *rhs;

  assignment_ast_vertex() : ast_vertex(ASSIGNMENT_ASTV), lhs(NULL), rhs(NULL) { }
  ~assignment_ast_vertex() {
    delete lhs;
    delete rhs;
  }

  virtual void unparse(ostream&,int);
};

/**
 ast vertex for scopes
 */
class scope_ast_vertex : public ast_vertex {
public:
/**
 symbol table
 */
  symbol_table *stab;
  list<type_data> *data_stack_types;
  scope_ast_vertex(astv_type type) : ast_vertex(type) { }
  ~scope_ast_vertex() {
    delete stab;
    delete data_stack_types;
  }

  virtual void unparse(ostream&,int);
};

/**   
 ast vertex for dimensions
 */
class dim_ast_vertex : public ast_vertex {
public:
  int dim;

  dim_ast_vertex(int dim) : ast_vertex(DIM_ASTV), dim(dim) { }
  ~dim_ast_vertex() { }
};



/**   
 ast 
 */
class ast {
public:
/**   
 unique root
 */
  ast_vertex* ast_root;
  ast() : ast_root(NULL) {};
  ~ast() { if (ast_root) delete ast_root; }
/** 
 unparser
 */
  void unparse(ostream&);

};

#endif
