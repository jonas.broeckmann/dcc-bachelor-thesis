#include <assert.h>
#include <iostream>
#include <cstdlib>
#include <sstream>
#include <cmath>
#include "dcc.hpp"
#include "adjoint_code_joint.hpp"
#include "symbol_table.hpp"
#include "util.hpp"

using namespace std;

extern symbol* find_symbol(const string& name);
extern void register_data_stack_type(type_data type);

static const unsigned int amode_adjoint = 1;
static const unsigned int amode_store_inputs = 2;
static const unsigned int amode_restore_inputs = 3;

static string stack_counter_suffix;
static string cs;
static string csc;

void adjoint_code_joint::build(const string& infile_name, ostream& outstream) {
  stack_counter_suffix = "_c";
  cs = "cs" + sac_var_prefix;
  csc = cs + stack_counter_suffix;
  register_data_stack_type(type_data::int_scalar); // ensure that an integer data stack exists
  vector<list<string> > adj_bbs;
  if (the_ast.ast_root) {
    unparse(the_ast.ast_root,outstream,0,adj_bbs,infile_name);
  }
}

static type_data adj_type(type_data type) {
  type.decomp_kind = type_data::decomp_kinds::none;
  return type;
}

static symbol adj_symbol(symbol *sym) {
  symbol adj_sym;
  adj_sym.name = adj_var_prefix + sym->name;
  adj_sym.kind = sym->kind;
  adj_sym.type = adj_type(sym->type);
  if (sym->dim != 0) {
    adj_sym.val = sym->val;
    adj_sym.dim = sym->dim;
    // cerr << "Adjoints on arrays aren't supported" << endl;
    // assert(false);
  } else {
    adj_sym.val = string("0");
  }
  return adj_sym;
}

static string sac_var(expr_ast_vertex *expr) {
  return expr->sac_var->name;
}

static string adj_sac_var(expr_ast_vertex *expr) {
  return adj_var_prefix + expr->sac_var->name;
}

static string sac_var_transposed(expr_ast_vertex *expr) {
  if (expr->get_res_type().is_vector()) {
    return sac_var(expr) + string(".transpose()");
  }
  return sac_var(expr);
}

static string ds_var(const type_data& type) {
  ostringstream stream;
  stream << "ds";
  type.unique_id(stream);
  return stream.str();
}
static string dsc_var(const type_data& type) {
  return ds_var(type) + stack_counter_suffix;
}

static string store(const type_data& type, const string& what) {
  ostringstream stream;
  stream << ds_var(type) << "[" << dsc_var(type) << "]" << "=" << what << "; ";
  stream << dsc_var(type) << "=" << dsc_var(type) << "+" << "1" << ";" << endl;
  return stream.str();
}
static string restore(const type_data& type, const string& what) {
  ostringstream stream;
  stream << dsc_var(type) << "=" << dsc_var(type) << "-" << "1" << "; ";
  stream << what << "=" << ds_var(type) << "[" << dsc_var(type) << "]" << ";" << endl;
  return stream.str();
}

static void unparse_stack(const type_data& type, const string& name, int size, scope_ast_vertex *scope, ostream& outstream, int indent) {
  string counter_name = name + stack_counter_suffix;
  symbol *s_sym, *sc_sym;
  s_sym = scope->stab->find(name);
  sc_sym = scope->stab->find(counter_name);
  if (!s_sym && !sc_sym) { // both need to be created
    s_sym = scope->stab->insert(name);
    s_sym->kind = symbol::kinds::global_var; 
    s_sym->type = type;
    s_sym->dim = size;
    sc_sym = scope->stab->insert(counter_name);
    sc_sym->kind = symbol::kinds::global_var; 
    sc_sym->type = type_data::int_scalar;
    sc_sym->val = string("0");
    indent(outstream); 
    s_sym->unparse_decl(outstream); outstream << "; ";
    sc_sym->unparse_decl(outstream); outstream << ";" << endl;
  } else if (s_sym && sc_sym) { // both exist -> check if they match
    assert(s_sym->kind == symbol::kinds::global_var);
    assert(s_sym->type.matches(type));
    assert(s_sym->dim >= size);
    assert(sc_sym->kind == symbol::kinds::global_var);
    assert(sc_sym->type.matches(type_data::int_scalar));
    assert(sc_sym->val == string("0"));
    assert(sc_sym->dim == 0);
  } else {
    cerr << "Stack " << name << " couldn't be created" << endl;
    exit(-3);
  }
}

void adjoint_code_joint::unparse(ast_vertex* v, ostream& outstream, int indent, vector<list<string>>& adj_bbs, const string& infile_name) {
  static bool new_bb=true;
  static int bb_idx=-1;
  static list<string> adj_bb;     // basic blocks
  static list<string> fs_sac;     // forward section SAC
  static list<string> fs_store;   // forward section store
  static list<string> rs_restore; // reverse section SAC/restore
  static list<string> rs_adjoint; // reverse section adjoint
  if (!v) return;
  list<ast_vertex*>::iterator it;
  switch (v->type) {
    case CODE_ASTV: {
      scope_ast_vertex *astv = static_cast<scope_ast_vertex*>(v);

      // unparse local symbol table
      it=v->children.begin();
      unparse(*it,outstream,indent,adj_bbs,infile_name);

      // control stack
      unparse_stack(type_data::int_scalar, cs, css, astv, outstream, indent);
      // data stacks
      for (list<type_data>::iterator stack_type = astv->data_stack_types->begin(); stack_type != astv->data_stack_types->end(); ++stack_type) {
        unparse_stack(*stack_type, ds_var(*stack_type), dss, astv, outstream, indent);
      }

      if (OPT_GENERATE_INCLUDES) {
        indent(outstream); outstream << "#include \"" << "declare_checkpoints.inc\"" << endl;
        indent(outstream); outstream << "#include \"" <<  infile_name <<"\"" << endl;
      }
      unparse(*(++it),outstream,indent,adj_bbs,infile_name);
      break;
    }
    case SEQUENCE_OF_SUBROUTINES_ASTV: {
      for (it=v->children.begin();it!=v->children.end();it++) {
        unparse((*it), outstream,indent,adj_bbs,infile_name);
      }
      break;
    }
    case SUBROUTINE_ASTV: {
      adj_bbs.clear();
      bb_idx=-1;

      it=v->children.begin();
      adj_symbol(static_cast<symbol_ast_vertex*>(*it)->sym).unparse_decl(outstream);
      string subroutine_name = static_cast<symbol_ast_vertex*>(*it)->sym->name;
      outstream << "(";
      unparse(*(++it),outstream,indent,adj_bbs,infile_name);
      outstream << ") {" << endl;
      indent++;
      // unparse local symbol table
      unparse(*(++it),outstream,indent,adj_bbs,infile_name);
      string save_csc_name = string("save_") + csc;
      indent(outstream); outstream << "int " << save_csc_name << "=0;" << endl;
      indent(outstream); outstream << save_csc_name << "=" << csc << ";" << endl;

      indent(outstream); outstream << "if (" << amode_var_name << "==" << amode_adjoint << ") {" << endl;
      indent++;
      // unparse augmented forward section
      indent(outstream); outstream << "// augmented forward section" << endl;
      unparse(*(++it),outstream,indent,adj_bbs,infile_name);

      if (OPT_GENERATE_INCLUDES) {
        indent(outstream); outstream << "#include \"" << subroutine_name << "_store_results.inc\"" << endl;
      }

      // unparse reverse section
      indent(outstream); outstream << "// reverse section" << endl;
      indent(outstream); outstream << "while (" << csc << " > " << save_csc_name << ") {" << endl;
      indent++;
      indent(outstream); outstream << csc << "=" << csc << "-1;" << endl; 
      for (size_t bb_i = 0; bb_i < adj_bbs.size(); bb_i++) {
        list<string>& bb = adj_bbs[bb_i];
        if (!bb.empty()) {
          indent(outstream); outstream << "if (" << cs << "[" << csc << "]==" << bb_i << ") {" << endl;
          indent++;
          for (list<string>::iterator j = bb.begin(); j != bb.end(); j++) {
            indent(outstream); outstream << *j;
          }
          indent--;
          indent(outstream); outstream << "}" << endl;
        }
      }
      indent--;
      indent(outstream); outstream << "}" << endl;
      if (OPT_GENERATE_INCLUDES) {
        indent(outstream); outstream << "#include \"" << subroutine_name << "_restore_results.inc\"" << endl;
      }
      indent--;
      indent(outstream); outstream << "}" << endl;

      // store inputs
      indent(outstream); outstream << "if (" << amode_var_name << "==" << amode_store_inputs << ") {" << endl;
      // user inserts code
      indent++;
      if (OPT_GENERATE_INCLUDES) {
        indent(outstream); outstream << "#include \"" << subroutine_name << "_store_inputs.inc\"" << endl;
      }
      indent(outstream); outstream << amode_var_name << "=" << amode_var_name << ";" << endl;
      indent--;
      indent(outstream); outstream << "}" << endl;
      // restore inputs
      indent(outstream); outstream << "if (" << amode_var_name << "==" << amode_restore_inputs << ") {" << endl;
      // user inserts code
      indent++;
      if (OPT_GENERATE_INCLUDES) {
        indent(outstream); outstream << "#include \"" << subroutine_name << "_restore_inputs.inc\"" << endl;
      }
      indent(outstream); outstream << amode_var_name << "=" << amode_var_name << ";" << endl;
      indent--;
      indent(outstream); outstream << "}" << endl;
      indent--;
      indent(outstream); outstream << "}" << endl;
      new_bb=true;
      break;
    }
    case SUBROUTINE_ARG_LIST_ASTV: {
      int s=1;
      outstream << "int " << amode_var_name << ", ";
      
      for (it=v->children.begin();it!=v->children.end();it++,s++) {
        symbol_ast_vertex *astv = static_cast<symbol_ast_vertex*>(*it);

        astv->sym->unparse_decl(outstream);
        if (astv->sym->type.is_active()) {
          outstream << ", ";
          adj_symbol(astv->sym).unparse_decl(outstream);
        }
        if (s<v->children.size()) outstream << ", ";
      }
      break;
    }
    case SEQUENCE_OF_DECLARATIONS_ASTV: {
      for (it=v->children.begin();it!=v->children.end();it++) {
        symbol_ast_vertex *astv = static_cast<symbol_ast_vertex*>(*it);
        indent(outstream);
        astv->sym->unparse_decl(outstream);
        outstream << ";" << endl;
        if (astv->sym->type.is_active()) {
          indent(outstream);
          adj_symbol(astv->sym).unparse_decl(outstream);
          outstream << ";" << endl;
        }
      }
      break;
    }
    case SEQUENCE_OF_STATEMENTS_ASTV: {
      for (it=v->children.begin();it!=v->children.end();it++)
        unparse((*it), outstream,indent,adj_bbs,infile_name);
      break;
    }
    case IF_STATEMENT_ASTV: {
      new_bb=true;
      indent(outstream);
      outstream << "if (";
      it=v->children.begin();
      (*it)->unparse(outstream,indent);
      outstream << ") {" << endl;
      indent++;
      unparse((*(++it)), outstream,indent,adj_bbs,infile_name);
      indent--;
      indent(outstream);
      outstream << "}";
      if (v->children.size()==3) {
        new_bb=true;
        outstream << " else {" << endl;
        indent++;
        unparse((*(++it)), outstream,indent,adj_bbs,infile_name);
        indent--;
        indent(outstream);
        outstream << "}";
      }
      outstream << endl;
      new_bb=true;
      break;
    }
    case WHILE_STATEMENT_ASTV: {
      new_bb=true;
      indent(outstream);
      outstream << "while (";
      it=v->children.begin();
      (*it)->unparse(outstream,indent);
      outstream << ") {" << endl;
      indent++;
      unparse((*(++it)), outstream,indent,adj_bbs,infile_name);
      indent--;
      indent(outstream);
      outstream << "}" << endl;
      new_bb=true;
      break;
    }
    case ASSIGNMENT_ASTV: {
      if (new_bb) { // push bb_idx
        new_bb=false;
        adj_bb.clear();
        indent(outstream);
        outstream << cs << "[" << csc << "]=" << ++bb_idx << "; ";
        outstream << csc << "=" << csc << "+1;" << endl;
      }

      assignment_ast_vertex *astv = static_cast<assignment_ast_vertex*>(v);
      type_data type = astv->lhs->sym->type;

      ostringstream os;
      astv->lhs->unparse(os, 0);
      string lhs_str = os.str();

      // generate SAC and "store code" for forward section and "restore code" and "adjoint code" for reverse section
      fs_sac.clear();
      fs_store.clear();
      rs_restore.clear();
      rs_adjoint.clear();
      if (type.is_active()) unparse(astv->rhs,outstream,indent,adj_bbs,infile_name);

      {
        // local adjoint
        if (type.is_active()) {
          symbol adj_sym = adj_symbol(astv->lhs->sym);
          memref_ast_vertex *adj_lhs;
          switch (astv->lhs->type) {
            case SCALAR_MEMREF_ASTV:
              adj_lhs = new memref_ast_vertex(&adj_sym);
              break;
            case ARRAY_MEMREF_ASTV:
              adj_lhs = new array_memref_ast_vertex(&adj_sym);
              static_cast<array_memref_ast_vertex*>(adj_lhs)->offset = static_cast<array_memref_ast_vertex*>(astv->lhs)->offset;
              break;
            default: assert(false); break;
          }
          ostringstream local_adjs;
          if (astv->rhs->get_res_type().is_active()) {
            local_adjs << adj_sac_var(astv->rhs) << "=";
            adj_lhs->unparse(local_adjs, 0);
            local_adjs << "; ";
          }
          adj_lhs->unparse(local_adjs, 0);
          local_adjs << "=";
          symbol::constant(adj_lhs->get_res_type(), "0").unparse_access(local_adjs);
          local_adjs << ";" << endl;
          rs_adjoint.push_front(local_adjs.str());
          delete adj_lhs;
        }
      }

      // augmented forward
      if (OPT_FS_SAC && type.is_active()) {
        list<string>::iterator fit;
        for (fit = fs_sac.begin(); fit != fs_sac.end(); fit++) { indent(outstream); outstream << *fit; }
        for (fit = fs_store.begin(); fit != fs_store.end(); fit++) { indent(outstream); outstream << *fit; }
      }
      indent(outstream); outstream << store(type, lhs_str); // store value
      if (OPT_FS_SAC && type.is_active()) {
        indent(outstream); outstream << lhs_str << "=" << sac_var(astv->rhs) << ";" << endl;
      } else {
        v->unparse(outstream, indent);
      }

      // reverse basic block
      list<string>::reverse_iterator rit;
      for (rit = rs_adjoint.rbegin(); rit != rs_adjoint.rend(); rit++) adj_bb.push_front(*rit);
      for (rit = rs_restore.rbegin(); rit != rs_restore.rend(); rit++) adj_bb.push_front(*rit);
      adj_bb.push_front(restore(type, lhs_str)); // restore overwritten value

      if (adj_bbs.size() <= bb_idx)
        adj_bbs.push_back(adj_bb);
      else
        adj_bbs[bb_idx] = adj_bb;
      break;
    }
    case SUBROUTINE_CALL_ASTV: {
      if (new_bb) { // push bb_idx
        new_bb=false;
        adj_bb.clear();
        indent(outstream);
        outstream << cs << "[" << csc << "]=" << ++bb_idx << "; "
                  << csc << "=" << csc << "+1;" << endl;
      }

      symbol adj_name = adj_symbol(static_cast<symbol_ast_vertex*>(child(v, 0))->sym);
      
      // generate SAC and "store code" for forward section and "restore code" and "adjoint code" for reverse section
      fs_sac.clear();
      fs_store.clear();
      rs_restore.clear();
      rs_adjoint.clear();

      {
        // store argument checkpoint
        ostringstream local_store;
        adj_name.unparse_access(local_store);
        local_store << "(" << amode_store_inputs << ", ";
        unparse(child(v, 1), local_store, indent, adj_bbs, infile_name);
        local_store << ");" << endl;
        fs_store.push_front(local_store.str());

        // restore argument checkpoint
        ostringstream local_sacs;
        adj_name.unparse_access(local_sacs);
        local_sacs << "(" << amode_restore_inputs << ", ";
        unparse(child(v, 1), local_sacs, indent, adj_bbs, infile_name);
        local_sacs << ");" << endl;
        rs_restore.push_back(local_sacs.str());

        // augmented forward sweep
        ostringstream local_adjs;
        adj_name.unparse_access(local_adjs);
        local_adjs << "(" << amode_adjoint << ", ";
        unparse(child(v, 1), local_adjs, indent, adj_bbs, infile_name);
        local_adjs << ");" << endl;
        rs_adjoint.push_front(local_adjs.str());
      }
      
      // augmented forward
      list<string>::iterator fit;
      for (fit = fs_sac.begin(); fit != fs_sac.end(); fit++) { indent(outstream); outstream << *fit; }
      for (fit = fs_store.begin(); fit != fs_store.end(); fit++) { indent(outstream); outstream << *fit; }
      v->unparse(outstream, indent);
      
      // reverse basic block
      list<string>::reverse_iterator rit;
      for (rit = rs_adjoint.rbegin(); rit != rs_adjoint.rend(); rit++) adj_bb.push_front(*rit);
      for (rit = rs_restore.rbegin(); rit != rs_restore.rend(); rit++) adj_bb.push_front(*rit);

      if (adj_bbs.size() <= bb_idx)
        adj_bbs.push_back(adj_bb);
      else
        adj_bbs[bb_idx] = adj_bb;
      break;
    }
    case SUBROUTINE_CALL_ARG_LIST_ASTV: {
      for (it = v->children.begin(); it != v->children.end(); it++) {
        symbol_ast_vertex *astv = static_cast<symbol_ast_vertex*>(*it);
        astv->unparse(outstream, indent);
        if (astv->sym->type.is_active()) {
          outstream << ", ";
          adj_symbol(astv->sym).unparse_access(outstream);
        }
        if (astv != v->children.back()) outstream << ", ";
      }
      break;
    }
    case ADD_EXPRESSION_ASTV: {
      binary_expr_ast_vertex *astv = static_cast<binary_expr_ast_vertex*>(v);
      // depth_first, post-order AST traversal
      unparse(astv->lhs,outstream,indent,adj_bbs,infile_name);
      unparse(astv->rhs,outstream,indent,adj_bbs,infile_name);
      // local single assignment code
      ostringstream local_sacs;
      local_sacs << sac_var(astv)
                 << "="
                 << sac_var(astv->lhs)
                 << "+"
                 << sac_var(astv->rhs)
                 << ";" << endl;
      string local_sac = local_sacs.str();
      fs_sac.push_back(local_sac);
      rs_restore.push_back(local_sac);
      // local adjoint
      ostringstream local_adjs;
      local_adjs << adj_sac_var(astv->lhs) 
                 << "="
                 << adj_sac_var(astv) 
                 << "; "
                 << adj_sac_var(astv->rhs) 
                 << "="
                 << adj_sac_var(astv)
                 << ";" << endl;
      rs_adjoint.push_front(local_adjs.str());
      break;
    }
    case SUB_EXPRESSION_ASTV: {
      binary_expr_ast_vertex *astv = static_cast<binary_expr_ast_vertex*>(v);
      // depth_first, post-order AST traversal
      unparse(astv->lhs,outstream,indent,adj_bbs,infile_name);
      unparse(astv->rhs,outstream,indent,adj_bbs,infile_name);
      // local single assignment code
      ostringstream local_sacs;
      local_sacs << sac_var(astv)
                 << "="
                 << sac_var(astv->lhs)
                 << "-"
                 << sac_var(astv->rhs)
                 << ";" << endl;
      string local_sac = local_sacs.str();
      fs_sac.push_back(local_sac);
      rs_restore.push_back(local_sac);
      // local adjoint
      ostringstream local_adjs;
      local_adjs << adj_sac_var(astv->lhs) 
                 << "="
                 << adj_sac_var(astv) 
                 << "; "
                 << adj_sac_var(astv->rhs) 
                 << "="
                 << "-"
                 << adj_sac_var(astv)
                 << ";" << endl;
      rs_adjoint.push_front(local_adjs.str());
      break;
    }
    case MUL_EXPRESSION_ASTV: {
      binary_expr_ast_vertex *astv = static_cast<binary_expr_ast_vertex*>(v);
      // depth_first, post-order AST traversal
      unparse(astv->lhs,outstream,indent,adj_bbs,infile_name);
      unparse(astv->rhs,outstream,indent,adj_bbs,infile_name);
      // local single assignment code
      ostringstream local_sacs;
      local_sacs << sac_var(astv)
                 << "="
                 << sac_var(astv->lhs)
                 << "*"
                 << sac_var(astv->rhs)
                 << ";" << endl;
      string local_sac = local_sacs.str();
      fs_sac.push_back(local_sac);
      rs_restore.push_back(local_sac);
      // local adjoint
      ostringstream local_adjs;
      if (astv->lhs->get_res_type().is_scalar() && astv->rhs->get_res_type().is_vector()) {
        local_adjs << adj_sac_var(astv->lhs)
                   << "="
                   << "("
                   << adj_sac_var(astv)
                   << "*"
                   << sac_var_transposed(astv->rhs)
                   << ").trace()" // for scalar use trace
                   << "; ";
      } else {
        local_adjs << adj_sac_var(astv->lhs)
                   << "="
                   << adj_sac_var(astv)
                   << "*"
                   << sac_var_transposed(astv->rhs)
                   << "; ";
      }
      if (astv->lhs->get_res_type().is_vector() && astv->rhs->get_res_type().is_scalar()) {
        local_adjs << adj_sac_var(astv->rhs)
                   << "="
                   << "("
                   << sac_var_transposed(astv->lhs)
                   << "*"
                   << adj_sac_var(astv)
                   << ").trace()" // for scalar use trace
                   << ";" << endl;
      } else {
        local_adjs << adj_sac_var(astv->rhs)
                   << "="
                   << sac_var_transposed(astv->lhs)
                   << "*"
                   << adj_sac_var(astv)
                   << ";" << endl;
      }
      rs_adjoint.push_front(local_adjs.str());
      break;
    }
    case DIV_EXPRESSION_ASTV: {
      binary_expr_ast_vertex *astv = static_cast<binary_expr_ast_vertex*>(v);
      // depth_first, post-order AST traversal
      unparse(astv->lhs,outstream,indent,adj_bbs,infile_name);
      unparse(astv->rhs,outstream,indent,adj_bbs,infile_name);
      // local single assignment code
      ostringstream local_sacs;
      local_sacs << sac_var(astv)
                 << "="
                 << sac_var(astv->lhs)
                 << "/"
                 << sac_var(astv->rhs)
                 << ";" << endl;
      string local_sac = local_sacs.str();
      fs_sac.push_back(local_sac);
      rs_restore.push_back(local_sac);
      // local adjoint
      ostringstream local_adjs;
      local_adjs << adj_sac_var(astv->lhs)
                 << "="
                 << adj_sac_var(astv)
                 << "/" 
                 << sac_var(astv->rhs)
                 << "; ";
      if (astv->lhs->get_res_type().is_vector()) {
        local_adjs << adj_sac_var(astv->rhs)
                   << "="
                   << "-"
                   << "("
                   << sac_var_transposed(astv->lhs)
                   << "*"
                   << adj_sac_var(astv)
                   << ").trace()" // for scalar use trace
                   << "/("
                   << sac_var(astv->rhs)
                   << "*"
                   << sac_var(astv->rhs)
                   << ")"
                   << ";" << endl;
      } else {
        local_adjs << adj_sac_var(astv->rhs)
                   << "="
                   << "-"
                   << sac_var(astv->lhs)
                   << "*" 
                   << adj_sac_var(astv)
                   << "/("
                   << sac_var(astv->rhs)
                   << "*"
                   << sac_var(astv->rhs)
                   << ")"
                   << ";" << endl;
      }
      rs_adjoint.push_front(local_adjs.str());
      break;
    }
    case SIN_EXPRESSION_ASTV: {
      unary_expr_ast_vertex *astv = static_cast<unary_expr_ast_vertex*>(v);
      // depth_first, post-order AST traversal
      unparse(astv->child,outstream,indent,adj_bbs,infile_name);
      // local single assignment code
      ostringstream local_sacs;
      local_sacs << sac_var(astv) 
                 << "=sin(" 
                 << sac_var(astv->child) 
                 << ");" << endl;
      string local_sac = local_sacs.str();
      fs_sac.push_back(local_sac);
      rs_restore.push_back(local_sac);
      // local adjoint
      ostringstream local_adjs;
      local_adjs << adj_sac_var(astv->child)
                 << "=cos("
                 << sac_var(astv->child)
                 << ")*"
                 << adj_sac_var(astv)
                 << ";" << endl;
      rs_adjoint.push_front(local_adjs.str());
      break;
    }
    case COS_EXPRESSION_ASTV: {
      unary_expr_ast_vertex *astv = static_cast<unary_expr_ast_vertex*>(v);
      // depth_first, post-order AST traversal
      unparse(astv->child,outstream,indent,adj_bbs,infile_name);
      // local single assignment code
      ostringstream local_sacs;
      local_sacs << sac_var(astv)
                 << "=cos("
                 << sac_var(astv->child)
                 << ");" << endl;
      string local_sac = local_sacs.str();
      fs_sac.push_back(local_sac);
      rs_restore.push_back(local_sac);
      // local adjoint
      ostringstream local_adjs;
      local_adjs << adj_sac_var(astv->child)
                 << "="
                 << "-sin("
                 << sac_var(astv->child)
                 << ")*"
                 << adj_sac_var(astv)
                 << ";" << endl;
      rs_adjoint.push_front(local_adjs.str());
      break;
    }
    case TAN_EXPRESSION_ASTV: {
      unary_expr_ast_vertex *astv = static_cast<unary_expr_ast_vertex*>(v);
      // depth_first, post-order AST traversal
      unparse(astv->child,outstream,indent,adj_bbs,infile_name);
      // local single assignment code
      ostringstream local_sacs;
      local_sacs << sac_var(astv)
                 << "=tan("
                 << sac_var(astv->child)
                 << ");" << endl;
      string local_sac = local_sacs.str();
      fs_sac.push_back(local_sac);
      rs_restore.push_back(local_sac);
      // local adjoint
      ostringstream local_adjs;
      local_adjs << adj_sac_var(astv->child)
                 << "=1/(cos("
                 << sac_var(astv->child)
                 << ")*"
                 << "cos("
                 << sac_var(astv->child)
                 << "))*"
                 << adj_sac_var(astv)
                 << ";" << endl;
      rs_adjoint.push_front(local_adjs.str());
      break;
    }
    case EXP_EXPRESSION_ASTV: {
      unary_expr_ast_vertex *astv = static_cast<unary_expr_ast_vertex*>(v);
      // depth_first, post-order AST traversal
      unparse(astv->child,outstream,indent,adj_bbs,infile_name);
      // local single assignment code
      ostringstream local_sacs;
      local_sacs << sac_var(astv)
                 << "=exp("
                 << sac_var(astv->child)
                 << ");" << endl;
      string local_sac = local_sacs.str();
      fs_sac.push_back(local_sac);
      rs_restore.push_back(local_sac);
      // local adjoint
      ostringstream local_adjs;
      local_adjs << adj_sac_var(astv->child)
                 << "="
                 << sac_var(astv)
                 << "*"
                 << adj_sac_var(astv)
                 << ";" << endl;
      rs_adjoint.push_front(local_adjs.str());
      break;
    }
    case SQRT_EXPRESSION_ASTV: {
      unary_expr_ast_vertex *astv = static_cast<unary_expr_ast_vertex*>(v);
      // depth_first, post-order AST traversal
      unparse(astv->child,outstream,indent,adj_bbs,infile_name);
      // local single assignment code
      ostringstream local_sacs;
      local_sacs << sac_var(astv)
                 << "=sqrt("
                 << sac_var(astv->child)
                 << ");" << endl;
      string local_sac = local_sacs.str();
      fs_sac.push_back(local_sac);
      rs_restore.push_back(local_sac);
      // local adjoint
      ostringstream local_adjs;
      local_adjs << adj_sac_var(astv->child)
                 << "=1/(2*sqrt("
                 << sac_var(astv->child)
                 << "))*"
                 << adj_sac_var(astv)
                 << ";" << endl;
      rs_adjoint.push_front(local_adjs.str());
      break;
    }
    case ATAN_EXPRESSION_ASTV: {
      unary_expr_ast_vertex *astv = static_cast<unary_expr_ast_vertex*>(v);
      // depth_first, post-order AST traversal
      unparse(astv->child,outstream,indent,adj_bbs,infile_name);
      // local single assignment code
      ostringstream local_sacs;
      local_sacs << sac_var(astv)
                 << "=atan("
                 << sac_var(astv->child)
                 << ");" << endl;
      string local_sac = local_sacs.str();
      fs_sac.push_back(local_sac);
      rs_restore.push_back(local_sac);
      // local adjoint
      ostringstream local_adjs;
      local_adjs << adj_sac_var(astv->child)
                 << "=1/(1+"
                 << sac_var(astv->child)
                 << "*"
                 << sac_var(astv->child)
                 << ")*"
                 << adj_sac_var(astv)
                 << ";" << endl;
      rs_adjoint.push_front(local_adjs.str());
      break;
    }
    case LOG_EXPRESSION_ASTV: {
      unary_expr_ast_vertex *astv = static_cast<unary_expr_ast_vertex*>(v);
      // depth_first, post-order AST traversal
      unparse(astv->child,outstream,indent,adj_bbs,infile_name);
      // local single assignment code
      ostringstream local_sacs;
      local_sacs << sac_var(astv)
                 << "=log("
                 << sac_var(astv->child)
                 << ");" << endl;
      string local_sac = local_sacs.str();
      fs_sac.push_back(local_sac);
      rs_restore.push_back(local_sac);
      // local adjoint
      ostringstream local_adjs;
      local_adjs << adj_sac_var(astv->child)
                 << "="
                 << adj_sac_var(astv)
                 << "/"
                 << sac_var(astv->child)
                 << ";" << endl;
      rs_adjoint.push_front(local_adjs.str());
      break;
    }
    // y=pow(x,z)
    case POW_EXPRESSION_ASTV: {
      binary_expr_ast_vertex *astv = static_cast<binary_expr_ast_vertex*>(v);
      // depth_first, post-order AST traversal
      unparse(astv->lhs,outstream,indent,adj_bbs,infile_name);
      unparse(astv->rhs,outstream,indent,adj_bbs,infile_name);
      // local single assignment code
      ostringstream local_sacs;
      local_sacs << sac_var(astv)
                 << "=pow("
                 << sac_var(astv->lhs)
                 << "," 
                 << sac_var(astv->rhs)
                 << ");" << endl;
      string local_sac = local_sacs.str();
      fs_sac.push_back(local_sac);
      rs_restore.push_back(local_sac);
      // local adjoint
      ostringstream local_adjs;
      if (astv->rhs->get_res_type().is_active()) {
        local_adjs << adj_sac_var(astv->rhs) // b_z
                   << "="
                   << adj_sac_var(astv->rhs) // b_z
                   << "+"
                   << sac_var(astv) // y
                   << "*log("
                   << sac_var(astv->lhs) // x
                   << ")*"
                   << adj_sac_var(astv) // b_y
                   << ";" << endl;
        rs_adjoint.push_front(local_adjs.str());
      }
      local_adjs.str(""); local_adjs.clear();
      local_adjs << adj_sac_var(astv->lhs) // b_x
                 << "="
                 << sac_var(astv->rhs) // z
                 << "*pow("
                 << sac_var(astv->lhs) // x
                 << ","
                 << sac_var(astv->rhs) // z
                 << "-1)*"
                 << adj_sac_var(astv) // b_y
                 << ";" << endl;
      rs_adjoint.push_front(local_adjs.str());
      break;
    }
    case PAR_EXPRESSION_ASTV: {
      unary_expr_ast_vertex *astv = static_cast<unary_expr_ast_vertex*>(v);
      // depth_first, post-order AST traversal
      unparse(astv->child,outstream,indent,adj_bbs,infile_name);
      // local single assignment code
      ostringstream local_sacs;
      local_sacs << sac_var(astv)
                 << "="
                 << sac_var(astv->child)
                 << ";" << endl;
      string local_sac = local_sacs.str();
      fs_sac.push_back(local_sac);
      rs_restore.push_back(local_sac);
      // local adjoint
      ostringstream local_adjs;
      local_adjs << adj_sac_var(astv->child)
                 << "="
                 << adj_sac_var(astv)
                 << ";" << endl;
      rs_adjoint.push_front(local_adjs.str());
      break;
    }
    case UPLUS_EXPRESSION_ASTV: {
      unary_expr_ast_vertex *astv = static_cast<unary_expr_ast_vertex*>(v);
      // depth_first, post-order AST traversal
      unparse(astv->child,outstream,indent,adj_bbs,infile_name);
      // local single assignment code
      ostringstream local_sacs;
      local_sacs << sac_var(astv)
                 << "="
                 << sac_var(astv->child)
                 << ";" << endl;
      string local_sac = local_sacs.str();
      fs_sac.push_back(local_sac);
      rs_restore.push_back(local_sac);
      // local adjoint
      ostringstream local_adjs;
      local_adjs << adj_sac_var(astv->child)
                 << "="
                 << adj_sac_var(astv)
                 << ";" << endl;
      rs_adjoint.push_front(local_adjs.str());
      break;
    }
    case UMINUS_EXPRESSION_ASTV: {
      unary_expr_ast_vertex *astv = static_cast<unary_expr_ast_vertex*>(v);
      // depth_first, post-order AST traversal
      unparse(astv->child,outstream,indent,adj_bbs,infile_name);
      // local single assignment code
      ostringstream local_sacs;
      local_sacs << sac_var(astv)
                 << "="
                 << "-"
                 << sac_var(astv->child)
                 << ";" << endl;
      string local_sac = local_sacs.str();
      fs_sac.push_back(local_sac);
      rs_restore.push_back(local_sac);
      // local adjoint
      ostringstream local_adjs;
      local_adjs << adj_sac_var(astv->child)
                 << "="
                 << "-"
                 << adj_sac_var(astv)
                 << ";" << endl;
      rs_adjoint.push_front(local_adjs.str());
      break;
    }
    case EVAL_EXPRESSION_ASTV: {
      unary_expr_ast_vertex *astv = static_cast<unary_expr_ast_vertex*>(v);
      // depth_first, post-order AST traversal
      unparse(astv->child,outstream,indent,adj_bbs,infile_name);
      // local single assignment code
      ostringstream local_sacs;
      local_sacs << sac_var(astv)
                 << "="
                 << sac_var(astv->child)
                 << ";" << endl;
      string local_sac = local_sacs.str();
      fs_sac.push_back(local_sac);
      rs_restore.push_back(local_sac);
      // local adjoint
      ostringstream local_adjs;
      local_adjs << adj_sac_var(astv->child)
                 << "="
                 << adj_sac_var(astv)
                 << ";" << endl;
      rs_adjoint.push_front(local_adjs.str());
      break;
    }
    case TRANSPOSE_EXPRESSION_ASTV: {
      unary_expr_ast_vertex *astv = static_cast<unary_expr_ast_vertex*>(v);
      if (astv->child->get_res_type().is_decomposition()) assert(false);
      // depth_first, post-order AST traversal
      unparse(astv->child,outstream,indent,adj_bbs,infile_name);
      // local single assignment code
      ostringstream local_sacs;
      local_sacs << sac_var(astv)
                 << "="
                 << sac_var(astv->child)
                 << ".transpose()"
                 << ";" << endl;
      string local_sac = local_sacs.str();
      fs_sac.push_back(local_sac);
      rs_restore.push_back(local_sac);
      // local adjoint
      ostringstream local_adjs;
      local_adjs << adj_sac_var(astv->child)
                 << "="
                 << adj_sac_var(astv)
                 << ".transpose()"
                 << ";" << endl;
      rs_adjoint.push_front(local_adjs.str());
      break;
    }
    case INVERSE_EXPRESSION_ASTV: {
      unary_expr_ast_vertex *astv = static_cast<unary_expr_ast_vertex*>(v);
      // depth_first, post-order AST traversal
      unparse(astv->child,outstream,indent,adj_bbs,infile_name);
      // local single assignment code
      ostringstream local_sacs;
      local_sacs << sac_var(astv)
                 << "="
                 << sac_var(astv->child)
                 << ".inverse()"
                 << ";" << endl;
      string local_sac = local_sacs.str();
      fs_sac.push_back(local_sac);
      if (OPT_FS_SAC_STORE) {
        fs_store.push_front(store(astv->get_res_type(), sac_var(astv)));
        rs_restore.push_back(restore(astv->get_res_type(), sac_var(astv)));
      } else {
        rs_restore.push_back(local_sac);
      }
      // local adjoint
      ostringstream local_adjs;
      local_adjs << adj_sac_var(astv->child)
                 << "="
                 << "-"
                 << sac_var_transposed(astv)
                 << "*"
                 << adj_sac_var(astv)
                 << "*"
                 << sac_var_transposed(astv)
                 << ";" << endl;
      rs_adjoint.push_front(local_adjs.str());
      break;
    }
    case TRACE_EXPRESSION_ASTV: {
      unary_expr_ast_vertex *astv = static_cast<unary_expr_ast_vertex*>(v);
      // depth_first, post-order AST traversal
      unparse(astv->child,outstream,indent,adj_bbs,infile_name);
      // local single assignment code
      ostringstream local_sacs;
      local_sacs << sac_var(astv)
                 << "="
                 << sac_var(astv->child)
                 << ".trace()"
                 << ";" << endl;
      string local_sac = local_sacs.str();
      fs_sac.push_back(local_sac);
      rs_restore.push_back(local_sac);
      // local adjoint
      ostringstream local_adjs;
      local_adjs << adj_sac_var(astv->child)
                 << "="
                 << adj_sac_var(astv)
                 << "*";
      symbol::constant(astv->child->get_res_type(), "1").unparse_access(local_adjs);
      local_adjs << ";" << endl;
      rs_adjoint.push_front(local_adjs.str());
      break;
    }
    case DETERMINANT_EXPRESSION_ASTV: {
      unary_expr_ast_vertex *astv = static_cast<unary_expr_ast_vertex*>(v);
      // depth_first, post-order AST traversal
      unparse(astv->child,outstream,indent,adj_bbs,infile_name);
      // local single assignment code
      ostringstream local_sacs;
      local_sacs << sac_var(astv)
                 << "="
                 << sac_var(astv->child)
                 << ".determinant()"
                 << ";" << endl;
      string local_sac = local_sacs.str();
      fs_sac.push_back(local_sac);
      if (OPT_FS_SAC_STORE) {
        fs_store.push_front(store(astv->get_res_type(), sac_var(astv)));
        rs_restore.push_back(restore(astv->get_res_type(), sac_var(astv)));
      } else {
        rs_restore.push_back(local_sac);
      }
      // local adjoint
      ostringstream local_adjs;
      local_adjs << adj_sac_var(astv->child)
                 << "="
                 << adj_sac_var(astv)
                 << "*"
                 << sac_var(astv)
                 << "*"
                 << sac_var(astv->child)
                 << ".transpose()"
                 << ".inverse()"
                 << ";" << endl;
      rs_adjoint.push_front(local_adjs.str());
      break;
    }
    case DOT_EXPRESSION_ASTV: {
      binary_expr_ast_vertex *astv = static_cast<binary_expr_ast_vertex*>(v);
      // depth_first, post-order AST traversal
      unparse(astv->lhs,outstream,indent,adj_bbs,infile_name);
      unparse(astv->rhs,outstream,indent,adj_bbs,infile_name);
      // local single assignment code
      ostringstream local_sacs;
      local_sacs << sac_var(astv)
                 << "="
                 << sac_var(astv->lhs)
                 << ".dot("
                 << sac_var(astv->rhs)
                 << ")"
                 << ";" << endl;
      string local_sac = local_sacs.str();
      fs_sac.push_back(local_sac);
      rs_restore.push_back(local_sac);
      // local adjoint
      ostringstream local_adjs;
      local_adjs << adj_sac_var(astv->lhs)
                 << "="
                 << adj_sac_var(astv)
                 << "*"
                 << sac_var(astv->rhs)
                 << "; ";
      local_adjs << adj_sac_var(astv->rhs)
                 << "="
                 << adj_sac_var(astv)
                 << "*"
                 << sac_var(astv->lhs)
                 << ";" << endl;
      rs_adjoint.push_front(local_adjs.str());
      break;
    }
    case SOLVE_EXPRESSION_ASTV: {
      binary_expr_ast_vertex *astv = static_cast<binary_expr_ast_vertex*>(v);

      expr_ast_vertex *decomp_source;
      string decomp_var, decomp_var_transposed;
      if (OPT_DECOMPOSITION_IN_SAC) {
        unparse(astv->lhs,outstream,indent,adj_bbs,infile_name);
        decomp_source = astv->lhs;
        decomp_var = sac_var(astv->lhs);
        decomp_var_transposed = sac_var(astv->lhs) + ".transpose()";
      } else {
        unary_expr_ast_vertex *orig_decomp = static_cast<unary_expr_ast_vertex*>(astv->lhs);
        unparse(orig_decomp->child,outstream,indent,adj_bbs,infile_name);
        string decomp_fun = type_data::decomposition_to_eigen_function(orig_decomp->get_res_type().decomp_kind);
        decomp_source = orig_decomp->child;
        decomp_var = sac_var(orig_decomp->child) + "." + decomp_fun + "()";
        decomp_var_transposed = sac_var(orig_decomp->child) + "." + decomp_fun + "()" + ".transpose()";
      }
      // depth_first, post-order AST traversal
      unparse(astv->rhs,outstream,indent,adj_bbs,infile_name);

      // local single assignment code
      ostringstream local_sacs;
      local_sacs << sac_var(astv)
                 << "="
                 << decomp_var // A
                 << ".solve("
                 << sac_var(astv->rhs) // b
                 << ")"
                 << ";" << endl;
      string local_sac = local_sacs.str();
      fs_sac.push_back(local_sac);
      if (OPT_FS_SAC_STORE) {
        fs_store.push_front(store(astv->get_res_type(), sac_var(astv)));
        rs_restore.push_back(restore(astv->get_res_type(), sac_var(astv)));
      } else {
        rs_restore.push_back(local_sac);
      }
      // local adjoint
      ostringstream local_adjs;
      local_adjs << adj_sac_var(astv->rhs) 
                 << "="
                 << decomp_var_transposed
                 << ".solve("
                 << adj_sac_var(astv)
                 << ")"
                 << "; "
                 << adj_sac_var(decomp_source) 
                 << "="
                 << "-"
                 << adj_sac_var(astv->rhs)
                 << "*"
                 << sac_var(astv)
                 << ".transpose()"
                 << ";" << endl;
      rs_adjoint.push_front(local_adjs.str());
      break;
    }
    case SOLVE_TRANSPOSED_EXPRESSION_ASTV: {
      binary_expr_ast_vertex *astv = static_cast<binary_expr_ast_vertex*>(v);

      expr_ast_vertex *decomp_source;
      string decomp_var, decomp_var_transposed;
      if (OPT_DECOMPOSITION_IN_SAC) {
        unparse(astv->lhs,outstream,indent,adj_bbs,infile_name);
        decomp_source = astv->lhs;
        decomp_var = sac_var(astv->lhs);
        decomp_var_transposed = sac_var(astv->lhs) + ".transpose()";
      } else {
        unary_expr_ast_vertex *orig_decomp = static_cast<unary_expr_ast_vertex*>(astv->lhs);
        unparse(orig_decomp->child,outstream,indent,adj_bbs,infile_name);
        string decomp_fun = type_data::decomposition_to_eigen_function(orig_decomp->get_res_type().decomp_kind);
        decomp_source = orig_decomp->child;
        decomp_var = sac_var(orig_decomp->child) + "." + decomp_fun + "()";
        decomp_var_transposed = sac_var(orig_decomp->child) + "." + decomp_fun + "()" + ".transpose()";
      }
      // depth_first, post-order AST traversal
      unparse(astv->rhs,outstream,indent,adj_bbs,infile_name);

      // local single assignment code
      ostringstream local_sacs;
      local_sacs << sac_var(astv)
                 << "="
                 << decomp_var_transposed // A
                 << ".solve("
                 << sac_var(astv->rhs) // b
                 << ")"
                 << ";" << endl;
      string local_sac = local_sacs.str();
      fs_sac.push_back(local_sac);
      if (OPT_FS_SAC_STORE) {
        fs_store.push_front(store(astv->get_res_type(), sac_var(astv)));
        rs_restore.push_back(restore(astv->get_res_type(), sac_var(astv)));
      } else {
        rs_restore.push_back(local_sac);
      }
      // local adjoint
      ostringstream local_adjs;
      local_adjs << adj_sac_var(astv->rhs) 
                 << "="
                 << decomp_var
                 << ".solve("
                 << adj_sac_var(astv)
                 << ")"
                 << "; "
                 << adj_sac_var(decomp_source) 
                 << "="
                 << "-"
                 << adj_sac_var(astv->rhs)
                 << "*"
                 << sac_var(astv)
                 << ".transpose()"
                 << ";" << endl;
      rs_adjoint.push_front(local_adjs.str());
      break;
    }
    case DECOMP_EXPRESSION_ASTV: {
      if (!OPT_DECOMPOSITION_IN_SAC) assert(false);
      unary_expr_ast_vertex *astv = static_cast<unary_expr_ast_vertex*>(v);
      // depth_first, post-order AST traversal
      unparse(astv->child,outstream,indent,adj_bbs,infile_name);
      // local single assignment code
      ostringstream local_sacs;
      local_sacs << sac_var(astv)
                 << "="
                 << sac_var(astv->child)
                 << "."
                 << type_data::decomposition_to_eigen_function(astv->get_res_type().decomp_kind)
                 << "()"
                 << ";" << endl;
      string local_sac = local_sacs.str();
      fs_sac.push_back(local_sac);
      if (OPT_FS_SAC_STORE) {
        fs_store.push_front(store(astv->get_res_type(), sac_var(astv)));
        rs_restore.push_back(restore(astv->get_res_type(), sac_var(astv)));
      } else {
        rs_restore.push_back(local_sac);
      }
      // local adjoint
      ostringstream local_adjs;
      local_adjs << adj_sac_var(astv->child)
                 << "="
                 << adj_sac_var(astv)
                 << ";" << endl;
      rs_adjoint.push_front(local_adjs.str());
      break;
    }
    case SCALAR_MEMREF_ASTV: {
      memref_ast_vertex *astv = static_cast<memref_ast_vertex*>(v);
      // local single assignment code
      ostringstream local_sacs;
      local_sacs << sac_var(astv) 
                 << "=";
      astv->unparse(local_sacs, 0); 
      local_sacs << ";" << endl;
      string local_sac = local_sacs.str();
      fs_sac.push_back(local_sac);
      rs_restore.push_back(local_sac);
      // local adjoint
      ostringstream local_adjs;
      if (astv->sym->type.is_active()) {
        symbol adj_sym = adj_symbol(astv->sym);
        memref_ast_vertex adj_astv(&adj_sym);

        adj_astv.unparse(local_adjs, 0);
        local_adjs << "=";
        adj_astv.unparse(local_adjs, 0);
        local_adjs << "+"
                   << adj_sac_var(astv)
                   << ";" << endl;
      }
      rs_adjoint.push_front(local_adjs.str());
      break;
    }
    case ARRAY_MEMREF_ASTV: {
      array_memref_ast_vertex *astv = static_cast<array_memref_ast_vertex*>(v);
      // local single assignment code
      ostringstream local_sacs;
      local_sacs << sac_var(astv)
                 << "=";
      astv->unparse(local_sacs, 0);
      local_sacs << ";" << endl;
      string local_sac = local_sacs.str();
      fs_sac.push_back(local_sac);
      rs_restore.push_back(local_sac);
      // local adjoint
      ostringstream local_adjs;
      if (astv->sym->type.is_active()) {
        symbol adj_sym = adj_symbol(astv->sym);
        array_memref_ast_vertex adj_astv(&adj_sym);
        adj_astv.offset = astv->offset;

        adj_astv.unparse(local_adjs, 0);
        local_adjs << "=";
        adj_astv.unparse(local_adjs, 0);
        local_adjs << "+"
                   << adj_sac_var(astv)
                   << ";" << endl;
      }
      rs_adjoint.push_front(local_adjs.str());
      break;
    }
    case CONSTANT_ASTV: {
      const_ast_vertex *astv = static_cast<const_ast_vertex*>(v);
      // local single assignment code
      ostringstream local_sacs;
      local_sacs << sac_var(astv)
                 << "=";
      astv->sym->unparse_access(local_sacs);
      local_sacs << ";" << endl;
      string local_sac = local_sacs.str();
      fs_sac.push_back(local_sac);
      rs_restore.push_back(local_sac);
      break;
    }
    default: {
      cerr << "Unknown ast vertex type " << v->type << endl;
      exit(-3);
      break;
    }
  }
}






