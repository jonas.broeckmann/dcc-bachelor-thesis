#include <assert.h>
#include <iostream>
#include <cstdlib>
#include "symbol_table.hpp"
#include "type.hpp"
#include "util.hpp"


symbol *symbol_table::insert(const string& name) {
  symbol *sym = find(name);
  if (sym) return sym;
  sym=new symbol;
  sym->name = name;
  tab.push_back(sym);
  return sym;
}

symbol *symbol_table::find(const string& name) {
  for (list<symbol*>::iterator it = tab.begin(); it != tab.end(); it++) {
    if ((*it)->name == name) return *it;
  }
  return NULL;
}


void symbol_table::unparse_decl(ostream& outstream, int indent) {
  list<symbol*>::iterator tab_it;
  for (tab_it=tab.begin();tab_it!=tab.end();tab_it++) {
    indent(outstream);
    (*tab_it)->unparse_decl(outstream);
    outstream << ";" << endl;
  }
}

static void unparse_val(ostream& outstream, symbol *sym) {
  if (sym->type.is_scalar()) {
    outstream << sym->val;
    return;
  }
  if (sym->type.is_vector()) {
    if (!sym->type.is_fixed_size()) {
      cerr << "ERROR: Constants currently only available for fixed-size matrices" << endl;
      assert(false);
    }
    if (sym->val == "0" || sym->val == "0.0" || sym->val == "Zero") {
      sym->type.unparse_val(outstream); outstream << "::Zero()";
      return;
    }
    if (sym->val == "1" || sym->val == "1.0" || sym->val == "Identity") {
      sym->type.unparse_val(outstream); outstream << "::Identity()";
      return;
    }
    cerr << "ERROR: Value '" << sym->val << "' not implemented for vectors" << endl;
    assert(false);
  }
  if (sym->type.is_decomposition()) {
    if (sym->val == "0" || sym->val == "0.0") {
      sym->type.unparse_val(outstream); outstream << "()";
      return;
    }
    cerr << "ERROR: Value '" << sym->val << "' not implemented for decompositions" << endl;
    assert(false);
  }
  cerr << "ERROR: Value '" << sym->val << "' not implemented for kind '" << static_cast<int>(sym->type.kind) << "'" << endl;
  assert(false);
}

void symbol::unparse_decl(ostream& outstream) {
  type.unparse(outstream);
  outstream << name;
  bool with_initialization;
  switch (kind) {
    case kinds::subroutine_name:
    case kinds::subroutine_arg: {
      with_initialization = false;
      break;
    }
    case kinds::local_var:
    case kinds::aux_var:
    case kinds::global_var: {
      with_initialization = true;
      break;
    }
    default: assert(false);
  }
  if (dim == 0) {
    if (with_initialization) {
      outstream << " = ";
      unparse_val(outstream, this);
    }
  } else if (dim > 0) {
    if (with_initialization) {
      outstream << "[" << dim << "]";
    } else {
      cerr << "ERROR: Arrays without initialization aren't supported" << endl;
      assert(false);
    }
  } else {
    cerr << "ERROR: Unknown array dimension" << endl;
    assert(false);
  }
}

void symbol::unparse_access(ostream& outstream) {
  if (name == "") { // no name, possibly constant
    unparse_val(outstream, this);
  } else {
    outstream << name;
  }
}

