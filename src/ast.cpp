#include <iostream>
#include <sstream>
#include <fstream>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <assert.h>
#include "ast.hpp"
#include "symbol_table.hpp"
#include "util.hpp"

using namespace std;

void ast_vertex::unparse(ostream& outstream, int indent) {
  if (!this) return; // bad
  list<ast_vertex*>::iterator it;
  switch (type) {
    case SEQUENCE_OF_SUBROUTINES_ASTV: {
      for (it = children.begin(); it != children.end(); it++) 
        (*it)->unparse(outstream, indent);
      break;
    }
    case SUBROUTINE_ARG_LIST_ASTV: {
      for (it = children.begin(); it != children.end(); it++) {
        static_cast<symbol_ast_vertex*>(*it)->sym->unparse_decl(outstream);
        if (*it != children.back()) outstream << ", ";
      }
      break;
    }
    case SEQUENCE_OF_DECLARATIONS_ASTV: {
      for (it = children.begin();it != children.end(); it++) {
        indent(outstream);
        static_cast<symbol_ast_vertex*>(*it)->sym->unparse_decl(outstream);
        outstream << ";" << endl;
      }
      break;
    }
    case SEQUENCE_OF_STATEMENTS_ASTV: {
      for (it = children.begin(); it!=children.end(); it++) 
        (*it)->unparse(outstream, indent);
      break;
    }
    case IF_STATEMENT_ASTV: {
      indent(outstream);
      outstream << "if (";
      it = children.begin();
      (*it)->unparse(outstream, indent);
      outstream << ") {" << endl;
      indent++;
      (*(++it))->unparse(outstream, indent);
      indent--;
      indent(outstream);
      outstream << "}";
      if (children.size() == 3) {
        outstream << " else {" << endl;
        indent++;
        (*(++it))->unparse(outstream, indent);
        indent--;
        indent(outstream);
        outstream << "}";
      }
      outstream << endl;
      break;
    }
    case WHILE_STATEMENT_ASTV: {
      indent(outstream);
      outstream << "while (";
      it = children.begin();
      (*it)->unparse(outstream, indent);
      outstream << ") {" << endl;
      indent++;
      (*(++it))->unparse(outstream, indent);
      indent--;
      indent(outstream);
      outstream << "}" << endl;
      break;
    }
    case SUBROUTINE_CALL_ASTV: {
      indent(outstream);
      child(this, 0)->unparse(outstream, indent);
      outstream << "(";
      for (it = child(this, 1)->children.begin(); it != child(this, 1)->children.end(); it++) {
        if (it != child(this, 1)->children.begin()) outstream << ", ";
        (*it)->unparse(outstream, indent);
      }
      outstream << ");" << endl;
      break;
    }
    default: {
      cerr << "Unknown ast vertex type " << type << endl;
      exit(-1);
      break;
    }
  }
}

#define DEBUG_PRECEDENCE false

void unary_expr_ast_vertex::unparse(ostream& outstream, int indent) {
  if (DEBUG_PRECEDENCE) outstream << "(";
  switch (type) {
    case PAR_EXPRESSION_ASTV: {
      outstream << "(";
      child->unparse(outstream, indent);
      outstream << ")";
      break;
    }
    case UPLUS_EXPRESSION_ASTV: {
      outstream << "+";
      child->unparse(outstream, indent);
      break;
    }
    case UMINUS_EXPRESSION_ASTV: {
      outstream << "-";
      child->unparse(outstream, indent);
      break;
    }
    case SIN_EXPRESSION_ASTV: {
      outstream << "sin(";
      child->unparse(outstream, indent);
      outstream << ")";
      break;
    }
    case COS_EXPRESSION_ASTV: {
      outstream << "cos(";
      child->unparse(outstream, indent);
      outstream << ")";
      break;
    }
    case EXP_EXPRESSION_ASTV: {
      outstream << "exp(";
      child->unparse(outstream, indent);
      outstream << ")";
      break;
    }
    case SQRT_EXPRESSION_ASTV: {
      outstream << "sqrt(";
      child->unparse(outstream, indent);
      outstream << ")";
      break;
    }
    case TAN_EXPRESSION_ASTV: {
      outstream << "tan(";
      child->unparse(outstream, indent);
      outstream << ")";
      break;
    }
    case ATAN_EXPRESSION_ASTV: {
      outstream << "atan(";
      child->unparse(outstream, indent);
      outstream << ")";
      break;
    }
    case LOG_EXPRESSION_ASTV: {
      outstream << "log(";
      child->unparse(outstream, indent);
      outstream << ")";
      break;
    }
    case EVAL_EXPRESSION_ASTV: {
      child->unparse(outstream, indent);
      outstream << "." << "eval" << "(" << ")";
      break;
    }
    case TRANSPOSE_EXPRESSION_ASTV: {
      if (child->get_res_type().is_decomposition()) assert(false);
      child->unparse(outstream, indent);
      outstream << "." << "transpose" << "(" << ")";
      break;
    }
    case INVERSE_EXPRESSION_ASTV: {
      child->unparse(outstream, indent);
      outstream << "." << "inverse" << "(" << ")";
      break;
    }
    case TRACE_EXPRESSION_ASTV: {
      child->unparse(outstream, indent);
      outstream << "." << "trace" << "(" << ")";
      break;
    }
    case DETERMINANT_EXPRESSION_ASTV: {
      child->unparse(outstream, indent);
      outstream << "." << "determinant" << "(" << ")";
      break;
    }
    case DECOMP_EXPRESSION_ASTV: {
      child->unparse(outstream, indent);
      outstream << "." << type_data::decomposition_to_eigen_function(get_res_type().decomp_kind) << "(" << ")";
      break;
    }
    default: {
      cerr << "Unknown unary expression type " << type << endl;
      exit(-1);
      break;
    }
  }
  if (DEBUG_PRECEDENCE) outstream << ")";
}

void binary_expr_ast_vertex::unparse(ostream& outstream, int indent) {
  if (DEBUG_PRECEDENCE) outstream << "(";
  switch (type) {
    case ADD_EXPRESSION_ASTV: {
      lhs->unparse(outstream, indent);
      outstream << " + ";
      rhs->unparse(outstream, indent);
      break;
    }
    case SUB_EXPRESSION_ASTV: {
      lhs->unparse(outstream, indent);
      outstream << " - ";
      rhs->unparse(outstream, indent);
      break;
    }
    case MUL_EXPRESSION_ASTV: {
      lhs->unparse(outstream, indent);
      outstream << " * ";
      rhs->unparse(outstream, indent);
      break;
    }
    case DIV_EXPRESSION_ASTV: {
      lhs->unparse(outstream, indent);
      outstream << " / ";
      rhs->unparse(outstream, indent);
      break;
    }
    case POW_EXPRESSION_ASTV: {
      outstream << "pow(";
      lhs->unparse(outstream, indent);
      outstream << ", ";
      rhs->unparse(outstream, indent);
      outstream << ")";
      break;
    }
    case DOT_EXPRESSION_ASTV: {
      lhs->unparse(outstream, indent);
      outstream << "." << "dot" << "(";
      rhs->unparse(outstream, indent);
      outstream << ")";
      break;
    }
    case SOLVE_EXPRESSION_ASTV: {
      lhs->unparse(outstream, indent);
      outstream << "." << "solve" << "(";
      rhs->unparse(outstream, indent);
      outstream << ")";
      break;
    }
    case SOLVE_TRANSPOSED_EXPRESSION_ASTV: {
      lhs->unparse(outstream, indent);
      outstream << "." << "transpose" << "(" << ")" << "." << "solve" << "(";
      rhs->unparse(outstream, indent);
      outstream << ")";
      break;
    }
    case CONDITION_LT_ASTV: {
      lhs->unparse(outstream, indent);
      outstream << " < ";
      rhs->unparse(outstream, indent);
      break;
    }
    case CONDITION_GT_ASTV: {
      lhs->unparse(outstream, indent);
      outstream << " > ";
      rhs->unparse(outstream, indent);
      break;
    }
    case CONDITION_EQ_ASTV: {
      lhs->unparse(outstream, indent);
      outstream << " == ";
      rhs->unparse(outstream, indent);
      break;
    }
    case CONDITION_NEQ_ASTV: {
      lhs->unparse(outstream, indent);
      outstream << " != ";
      rhs->unparse(outstream, indent);
      break;
    }
    case CONDITION_LE_ASTV: {
      lhs->unparse(outstream, indent);
      outstream << " <= ";
      rhs->unparse(outstream, indent);
      break;
    }
    case CONDITION_GE_ASTV: {
      lhs->unparse(outstream, indent);
      outstream << " >= ";
      rhs->unparse(outstream, indent);
      break;
    }
    default: {
      cerr << "Unknown binary expression type " << type << endl;
      exit(-1);
      break;
    }
  }
  if (DEBUG_PRECEDENCE) outstream << ")";
}

void symbol_ast_vertex::unparse(ostream& outstream, int indent) {
  sym->unparse_access(outstream);
}

void array_memref_ast_vertex::unparse(ostream& outstream, int indent) {
  sym->unparse_access(outstream);
  outstream << "[";
  offset->unparse_access(outstream);
  outstream << "]";
}

void type_ast_vertex::unparse(ostream& outstream, int indent) {
  assert(false);
}

void assignment_ast_vertex::unparse(ostream& outstream, int indent) {
  indent(outstream);
  lhs->unparse(outstream, indent);
  outstream << " = ";
  rhs->unparse(outstream, indent);
  outstream << ";" << endl;
}

void scope_ast_vertex::unparse(ostream& outstream, int indent) {
  list<ast_vertex*>::iterator it;
  switch (type) {
    case CODE_ASTV: {
      it = children.begin();
      (*it)->unparse(outstream, indent);
      (*(++it))->unparse(outstream, indent);
      break;
    }
    case SUBROUTINE_ASTV: {
      it = children.begin();
      static_cast<symbol_ast_vertex*>(*it)->sym->unparse_decl(outstream);
      outstream << "(";
      (*(++it))->unparse(outstream, indent);
      outstream << ") {" << endl;
      indent++;
      (*(++it))->unparse(outstream, indent);
      (*(++it))->unparse(outstream, indent);
      indent--;
      outstream << "}" << endl;
      break;
    }
    default: {
      cerr << "Unknown scope type " << type << endl;
      exit(-1);
      break;
    }
  }
}

void ast::unparse(ostream& outstream) {
  if (ast_root) ast_root->unparse(outstream, 0);
}
