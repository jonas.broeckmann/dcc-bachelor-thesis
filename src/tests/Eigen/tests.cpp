
void test_const0(Matrix3d& y) {
    y = Matrix3d::Zero();
}

void test_const1(Matrix3d& y) {
    y = Matrix3d::Identity();
}

void test_assign(Matrix3d& a, Matrix3d& y) {
    y = a;
}

void test_paren(Matrix3d& a, Matrix3d& y) {
    y = (a);
}

void test_uplus(Matrix3d& a, Matrix3d& y) {
    y = +a;
}

void test_uminus(Matrix3d& a, Matrix3d& y) {
    y = -a;
}

void test_eval(Matrix<double, 2, 3>& a, Matrix<double, 2, 3>& y) {
    y = a.eval();
}

void test_transpose(Matrix<double, 2, 3>& a, Matrix<double, 3, 2>& y) {
    y = a.transpose();
}

void test_inverse(Matrix3d& a, Matrix3d& y) {
    y = a.inverse();
}

void test_trace(Matrix3d& a, double& y) {
    y = a.trace();
}

void test_determinant(Matrix3d& a, double& y) {
    y = a.determinant();
}

void test_add(Matrix3d& a, Matrix3d& b, Matrix3d& y) {
    y = a + b;
}

void test_sub(Matrix3d& a, Matrix3d& b, Matrix3d& y) {
    y = a - b;
}

void test_mult(Matrix<double, 2, 3>& a, Matrix<double, 3, 4>& b, Matrix<double, 2, 4>& y) {
    y = a * b;
}

void test_scalar_mult1(double a, Matrix<double, 2, 3>& b, Matrix<double, 2, 3>& y) {
    y = a * b;
}

void test_scalar_mult2(Matrix<double, 2, 3>& a, double b, Matrix<double, 2, 3>& y) {
    y = a * b;
}

void test_scalar_div(Matrix<double, 2, 3>& a, double b, Matrix<double, 2, 3>& y) {
    y = a / b;
}

void test_dot(Vector3d& a, Vector3d& b, double& y) {
    y = a.dot(b);
}

void test_solve(Matrix<double, 2, 3>& a, Vector2d& b, Vector3d& x) {
    x = a.lu().solve(b);
}

void test_solve_transposed(Matrix<double, 3, 2>& a, Vector2d& b, Vector3d& x) {
    x = a.lu().transpose().solve(b);
}
