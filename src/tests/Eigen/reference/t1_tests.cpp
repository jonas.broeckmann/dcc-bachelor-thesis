void t1_test_const0(Matrix3d& y, Matrix3d& t1_y) {
    Matrix3d v1f33_0 = Matrix3d::Zero();
    Matrix3d t1_v1f33_0 = Matrix3d::Zero();
    t1_v1f33_0=Matrix3d::Zero();
    v1f33_0=Matrix3d::Zero();
    t1_y=t1_v1f33_0;
    y=v1f33_0;
}
void t1_test_const1(Matrix3d& y, Matrix3d& t1_y) {
    Matrix3d v1f33_0 = Matrix3d::Zero();
    Matrix3d t1_v1f33_0 = Matrix3d::Zero();
    t1_v1f33_0=Matrix3d::Zero();
    v1f33_0=Matrix3d::Identity();
    t1_y=t1_v1f33_0;
    y=v1f33_0;
}
void t1_test_assign(Matrix3d& a, Matrix3d& t1_a, Matrix3d& y, Matrix3d& t1_y) {
    Matrix3d v1f33_0 = Matrix3d::Zero();
    Matrix3d t1_v1f33_0 = Matrix3d::Zero();
    t1_v1f33_0=t1_a;
    v1f33_0=a;
    t1_y=t1_v1f33_0;
    y=v1f33_0;
}
void t1_test_paren(Matrix3d& a, Matrix3d& t1_a, Matrix3d& y, Matrix3d& t1_y) {
    Matrix3d v1f33_0 = Matrix3d::Zero();
    Matrix3d t1_v1f33_0 = Matrix3d::Zero();
    Matrix3d v1f33_1 = Matrix3d::Zero();
    Matrix3d t1_v1f33_1 = Matrix3d::Zero();
    t1_v1f33_0=t1_a;
    v1f33_0=a;
    t1_v1f33_1=t1_v1f33_0;
    v1f33_1=v1f33_0;
    t1_y=t1_v1f33_1;
    y=v1f33_1;
}
void t1_test_uplus(Matrix3d& a, Matrix3d& t1_a, Matrix3d& y, Matrix3d& t1_y) {
    Matrix3d v1f33_0 = Matrix3d::Zero();
    Matrix3d t1_v1f33_0 = Matrix3d::Zero();
    Matrix3d v1f33_1 = Matrix3d::Zero();
    Matrix3d t1_v1f33_1 = Matrix3d::Zero();
    t1_v1f33_0=t1_a;
    v1f33_0=a;
    t1_v1f33_1=t1_v1f33_0;
    v1f33_1=v1f33_0;
    t1_y=t1_v1f33_1;
    y=v1f33_1;
}
void t1_test_uminus(Matrix3d& a, Matrix3d& t1_a, Matrix3d& y, Matrix3d& t1_y) {
    Matrix3d v1f33_0 = Matrix3d::Zero();
    Matrix3d t1_v1f33_0 = Matrix3d::Zero();
    Matrix3d v1f33_1 = Matrix3d::Zero();
    Matrix3d t1_v1f33_1 = Matrix3d::Zero();
    t1_v1f33_0=t1_a;
    v1f33_0=a;
    t1_v1f33_1=-t1_v1f33_0;
    v1f33_1=-v1f33_0;
    t1_y=t1_v1f33_1;
    y=v1f33_1;
}
void t1_test_eval(Matrix<double,2,3>& a, Matrix<double,2,3>& t1_a, Matrix<double,2,3>& y, Matrix<double,2,3>& t1_y) {
    Matrix<double,2,3> v1f23_0 = Matrix<double,2,3>::Zero();
    Matrix<double,2,3> t1_v1f23_0 = Matrix<double,2,3>::Zero();
    Matrix<double,2,3> v1f23_1 = Matrix<double,2,3>::Zero();
    Matrix<double,2,3> t1_v1f23_1 = Matrix<double,2,3>::Zero();
    t1_v1f23_0=t1_a;
    v1f23_0=a;
    t1_v1f23_1=t1_v1f23_0;
    v1f23_1=v1f23_0;
    t1_y=t1_v1f23_1;
    y=v1f23_1;
}
void t1_test_transpose(Matrix<double,2,3>& a, Matrix<double,2,3>& t1_a, Matrix<double,3,2>& y, Matrix<double,3,2>& t1_y) {
    Matrix<double,2,3> v1f23_0 = Matrix<double,2,3>::Zero();
    Matrix<double,2,3> t1_v1f23_0 = Matrix<double,2,3>::Zero();
    Matrix<double,3,2> v1f32_1 = Matrix<double,3,2>::Zero();
    Matrix<double,3,2> t1_v1f32_1 = Matrix<double,3,2>::Zero();
    t1_v1f23_0=t1_a;
    v1f23_0=a;
    t1_v1f32_1=t1_v1f23_0.transpose();
    v1f32_1=v1f23_0.transpose();
    t1_y=t1_v1f32_1;
    y=v1f32_1;
}
void t1_test_inverse(Matrix3d& a, Matrix3d& t1_a, Matrix3d& y, Matrix3d& t1_y) {
    Matrix3d v1f33_0 = Matrix3d::Zero();
    Matrix3d t1_v1f33_0 = Matrix3d::Zero();
    Matrix3d v1f33_1 = Matrix3d::Zero();
    Matrix3d t1_v1f33_1 = Matrix3d::Zero();
    t1_v1f33_0=t1_a;
    v1f33_0=a;
    v1f33_1=v1f33_0.inverse();
    t1_v1f33_1=-v1f33_1*t1_v1f33_0*v1f33_1;
    t1_y=t1_v1f33_1;
    y=v1f33_1;
}
void t1_test_trace(Matrix3d& a, Matrix3d& t1_a, double& y, double& t1_y) {
    Matrix3d v1f33_0 = Matrix3d::Zero();
    Matrix3d t1_v1f33_0 = Matrix3d::Zero();
    double v1f_1 = 0;
    double t1_v1f_1 = 0;
    t1_v1f33_0=t1_a;
    v1f33_0=a;
    t1_v1f_1=t1_v1f33_0.trace();
    v1f_1=v1f33_0.trace();
    t1_y=t1_v1f_1;
    y=v1f_1;
}
void t1_test_determinant(Matrix3d& a, Matrix3d& t1_a, double& y, double& t1_y) {
    Matrix3d v1f33_0 = Matrix3d::Zero();
    Matrix3d t1_v1f33_0 = Matrix3d::Zero();
    double v1f_1 = 0;
    double t1_v1f_1 = 0;
    t1_v1f33_0=t1_a;
    v1f33_0=a;
v1f_1=v1f33_0.determinant();
    t1_v1f_1=v1f_1*(v1f33_0.inverse()*t1_v1f33_0).trace();
        t1_y=t1_v1f_1;
    y=v1f_1;
}
void t1_test_add(Matrix3d& a, Matrix3d& t1_a, Matrix3d& b, Matrix3d& t1_b, Matrix3d& y, Matrix3d& t1_y) {
    Matrix3d v1f33_0 = Matrix3d::Zero();
    Matrix3d t1_v1f33_0 = Matrix3d::Zero();
    Matrix3d v1f33_1 = Matrix3d::Zero();
    Matrix3d t1_v1f33_1 = Matrix3d::Zero();
    Matrix3d v1f33_2 = Matrix3d::Zero();
    Matrix3d t1_v1f33_2 = Matrix3d::Zero();
    t1_v1f33_0=t1_a;
    v1f33_0=a;
    t1_v1f33_1=t1_b;
    v1f33_1=b;
    t1_v1f33_2=t1_v1f33_0+t1_v1f33_1;
    v1f33_2=v1f33_0+v1f33_1;
    t1_y=t1_v1f33_2;
    y=v1f33_2;
}
void t1_test_sub(Matrix3d& a, Matrix3d& t1_a, Matrix3d& b, Matrix3d& t1_b, Matrix3d& y, Matrix3d& t1_y) {
    Matrix3d v1f33_0 = Matrix3d::Zero();
    Matrix3d t1_v1f33_0 = Matrix3d::Zero();
    Matrix3d v1f33_1 = Matrix3d::Zero();
    Matrix3d t1_v1f33_1 = Matrix3d::Zero();
    Matrix3d v1f33_2 = Matrix3d::Zero();
    Matrix3d t1_v1f33_2 = Matrix3d::Zero();
    t1_v1f33_0=t1_a;
    v1f33_0=a;
    t1_v1f33_1=t1_b;
    v1f33_1=b;
    t1_v1f33_2=t1_v1f33_0-t1_v1f33_1;
    v1f33_2=v1f33_0-v1f33_1;
    t1_y=t1_v1f33_2;
    y=v1f33_2;
}
void t1_test_mult(Matrix<double,2,3>& a, Matrix<double,2,3>& t1_a, Matrix<double,3,4>& b, Matrix<double,3,4>& t1_b, Matrix<double,2,4>& y, Matrix<double,2,4>& t1_y) {
    Matrix<double,2,3> v1f23_0 = Matrix<double,2,3>::Zero();
    Matrix<double,2,3> t1_v1f23_0 = Matrix<double,2,3>::Zero();
    Matrix<double,3,4> v1f34_1 = Matrix<double,3,4>::Zero();
    Matrix<double,3,4> t1_v1f34_1 = Matrix<double,3,4>::Zero();
    Matrix<double,2,4> v1f24_2 = Matrix<double,2,4>::Zero();
    Matrix<double,2,4> t1_v1f24_2 = Matrix<double,2,4>::Zero();
    t1_v1f23_0=t1_a;
    v1f23_0=a;
    t1_v1f34_1=t1_b;
    v1f34_1=b;
    t1_v1f24_2=t1_v1f23_0*v1f34_1+v1f23_0*t1_v1f34_1;
    v1f24_2=v1f23_0*v1f34_1;
    t1_y=t1_v1f24_2;
    y=v1f24_2;
}
void t1_test_scalar_mult1(double a, double t1_a, Matrix<double,2,3>& b, Matrix<double,2,3>& t1_b, Matrix<double,2,3>& y, Matrix<double,2,3>& t1_y) {
    double v1f_0 = 0;
    double t1_v1f_0 = 0;
    Matrix<double,2,3> v1f23_1 = Matrix<double,2,3>::Zero();
    Matrix<double,2,3> t1_v1f23_1 = Matrix<double,2,3>::Zero();
    Matrix<double,2,3> v1f23_2 = Matrix<double,2,3>::Zero();
    Matrix<double,2,3> t1_v1f23_2 = Matrix<double,2,3>::Zero();
    t1_v1f_0=t1_a;
    v1f_0=a;
    t1_v1f23_1=t1_b;
    v1f23_1=b;
    t1_v1f23_2=t1_v1f_0*v1f23_1+v1f_0*t1_v1f23_1;
    v1f23_2=v1f_0*v1f23_1;
    t1_y=t1_v1f23_2;
    y=v1f23_2;
}
void t1_test_scalar_mult2(Matrix<double,2,3>& a, Matrix<double,2,3>& t1_a, double b, double t1_b, Matrix<double,2,3>& y, Matrix<double,2,3>& t1_y) {
    Matrix<double,2,3> v1f23_0 = Matrix<double,2,3>::Zero();
    Matrix<double,2,3> t1_v1f23_0 = Matrix<double,2,3>::Zero();
    double v1f_1 = 0;
    double t1_v1f_1 = 0;
    Matrix<double,2,3> v1f23_2 = Matrix<double,2,3>::Zero();
    Matrix<double,2,3> t1_v1f23_2 = Matrix<double,2,3>::Zero();
    t1_v1f23_0=t1_a;
    v1f23_0=a;
    t1_v1f_1=t1_b;
    v1f_1=b;
    t1_v1f23_2=t1_v1f23_0*v1f_1+v1f23_0*t1_v1f_1;
    v1f23_2=v1f23_0*v1f_1;
    t1_y=t1_v1f23_2;
    y=v1f23_2;
}
void t1_test_scalar_div(Matrix<double,2,3>& a, Matrix<double,2,3>& t1_a, double b, double t1_b, Matrix<double,2,3>& y, Matrix<double,2,3>& t1_y) {
    Matrix<double,2,3> v1f23_0 = Matrix<double,2,3>::Zero();
    Matrix<double,2,3> t1_v1f23_0 = Matrix<double,2,3>::Zero();
    double v1f_1 = 0;
    double t1_v1f_1 = 0;
    Matrix<double,2,3> v1f23_2 = Matrix<double,2,3>::Zero();
    Matrix<double,2,3> t1_v1f23_2 = Matrix<double,2,3>::Zero();
    t1_v1f23_0=t1_a;
    v1f23_0=a;
    t1_v1f_1=t1_b;
    v1f_1=b;
    t1_v1f23_2=t1_v1f23_0/v1f_1-v1f23_0*t1_v1f_1/(v1f_1*v1f_1);
    v1f23_2=v1f23_0/v1f_1;
    t1_y=t1_v1f23_2;
    y=v1f23_2;
}
void t1_test_dot(Vector3d& a, Vector3d& t1_a, Vector3d& b, Vector3d& t1_b, double& y, double& t1_y) {
    Vector3d v1f31_0 = Vector3d::Zero();
    Vector3d t1_v1f31_0 = Vector3d::Zero();
    Vector3d v1f31_1 = Vector3d::Zero();
    Vector3d t1_v1f31_1 = Vector3d::Zero();
    double v1f_2 = 0;
    double t1_v1f_2 = 0;
    t1_v1f31_0=t1_a;
    v1f31_0=a;
    t1_v1f31_1=t1_b;
    v1f31_1=b;
    t1_v1f_2=t1_v1f31_0.dot(v1f31_1)+v1f31_0.dot(t1_v1f31_1);
    v1f_2=v1f31_0.dot(v1f31_1);
    t1_y=t1_v1f_2;
    y=v1f_2;
}
void t1_test_solve(Matrix<double,2,3>& a, Matrix<double,2,3>& t1_a, Vector2d& b, Vector2d& t1_b, Vector3d& x, Vector3d& t1_x) {
    Matrix<double,2,3> v1f23_0 = Matrix<double,2,3>::Zero();
    Matrix<double,2,3> t1_v1f23_0 = Matrix<double,2,3>::Zero();
    PartialPivLU<Matrix<double,2,3>> v1f23d1_1 = PartialPivLU<Matrix<double,2,3>>();
    Matrix<double,2,3> t1_v1f23d1_1 = Matrix<double,2,3>::Zero();
    Vector2d v1f21_2 = Vector2d::Zero();
    Vector2d t1_v1f21_2 = Vector2d::Zero();
    Vector3d v1f31_3 = Vector3d::Zero();
    Vector3d t1_v1f31_3 = Vector3d::Zero();
    t1_v1f23_0=t1_a;
    v1f23_0=a;
    t1_v1f23d1_1=t1_v1f23_0;
    v1f23d1_1=v1f23_0.partialPivLu();
    t1_v1f21_2=t1_b;
    v1f21_2=b;
    v1f31_3=v1f23d1_1.solve(v1f21_2);
    t1_v1f31_3=v1f23d1_1.solve(-t1_v1f23d1_1*v1f31_3)+v1f23d1_1.solve(t1_v1f21_2);
    t1_x=t1_v1f31_3;
    x=v1f31_3;
}
void t1_test_solve_transposed(Matrix<double,3,2>& a, Matrix<double,3,2>& t1_a, Vector2d& b, Vector2d& t1_b, Vector3d& x, Vector3d& t1_x) {
    Matrix<double,3,2> v1f32_0 = Matrix<double,3,2>::Zero();
    Matrix<double,3,2> t1_v1f32_0 = Matrix<double,3,2>::Zero();
    PartialPivLU<Matrix<double,3,2>> v1f32d1_1 = PartialPivLU<Matrix<double,3,2>>();
    Matrix<double,3,2> t1_v1f32d1_1 = Matrix<double,3,2>::Zero();
    Vector2d v1f21_2 = Vector2d::Zero();
    Vector2d t1_v1f21_2 = Vector2d::Zero();
    Vector3d v1f31_3 = Vector3d::Zero();
    Vector3d t1_v1f31_3 = Vector3d::Zero();
    t1_v1f32_0=t1_a;
    v1f32_0=a;
    t1_v1f32d1_1=t1_v1f32_0;
    v1f32d1_1=v1f32_0.partialPivLu();
    t1_v1f21_2=t1_b;
    v1f21_2=b;
    v1f31_3=v1f32d1_1.transpose().solve(v1f21_2);
    t1_v1f31_3=v1f32d1_1.transpose().solve(-t1_v1f32d1_1*v1f31_3).eval()+v1f32d1_1.transpose().solve(t1_v1f21_2).eval();
    t1_x=t1_v1f31_3;
    x=v1f31_3;
}
