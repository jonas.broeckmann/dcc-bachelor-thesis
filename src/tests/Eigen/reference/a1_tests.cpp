int csv1[1000000]; int csv1_c = 0;
Matrix3d dsf33[100000]; int dsf33_c = 0;
Matrix<double,2,3> dsf23[100000]; int dsf23_c = 0;
Matrix<double,3,2> dsf32[100000]; int dsf32_c = 0;
double dsf[100000]; int dsf_c = 0;
Matrix<double,2,4> dsf24[100000]; int dsf24_c = 0;
PartialPivLU<Matrix<double,2,3>> dsf23d1[100000]; int dsf23d1_c = 0;
Vector3d dsf31[100000]; int dsf31_c = 0;
PartialPivLU<Matrix<double,3,2>> dsf32d1[100000]; int dsf32d1_c = 0;
#include "declare_checkpoints.inc"
#include "tests.cpp"
void a1_test_const0(int a1_mode, Matrix3d& y, Matrix3d& a1_y) {
    Matrix3d v1f33_0 = Matrix3d::Zero();
    Matrix3d a1_v1f33_0 = Matrix3d::Zero();
    int save_csv1_c=0;
    save_csv1_c=csv1_c;
    if (a1_mode==1) {
        // augmented forward section
        csv1[csv1_c]=0; csv1_c=csv1_c+1;
        v1f33_0=Matrix3d::Zero();
        dsf33[dsf33_c]=y; dsf33_c=dsf33_c+1;
        y=v1f33_0;
        #include "test_const0_store_results.inc"
        // reverse section
        while (csv1_c > save_csv1_c) {
            csv1_c=csv1_c-1;
            if (csv1[csv1_c]==0) {
                dsf33_c=dsf33_c-1; y=dsf33[dsf33_c];
                v1f33_0=Matrix3d::Zero();
                a1_v1f33_0=a1_y; a1_y=Matrix3d::Zero();
            }
        }
        #include "test_const0_restore_results.inc"
    }
    if (a1_mode==2) {
        #include "test_const0_store_inputs.inc"
        a1_mode=a1_mode;
    }
    if (a1_mode==3) {
        #include "test_const0_restore_inputs.inc"
        a1_mode=a1_mode;
    }
}
void a1_test_const1(int a1_mode, Matrix3d& y, Matrix3d& a1_y) {
    Matrix3d v1f33_0 = Matrix3d::Zero();
    Matrix3d a1_v1f33_0 = Matrix3d::Zero();
    int save_csv1_c=0;
    save_csv1_c=csv1_c;
    if (a1_mode==1) {
        // augmented forward section
        csv1[csv1_c]=0; csv1_c=csv1_c+1;
        v1f33_0=Matrix3d::Identity();
        dsf33[dsf33_c]=y; dsf33_c=dsf33_c+1;
        y=v1f33_0;
        #include "test_const1_store_results.inc"
        // reverse section
        while (csv1_c > save_csv1_c) {
            csv1_c=csv1_c-1;
            if (csv1[csv1_c]==0) {
                dsf33_c=dsf33_c-1; y=dsf33[dsf33_c];
                v1f33_0=Matrix3d::Identity();
                a1_v1f33_0=a1_y; a1_y=Matrix3d::Zero();
            }
        }
        #include "test_const1_restore_results.inc"
    }
    if (a1_mode==2) {
        #include "test_const1_store_inputs.inc"
        a1_mode=a1_mode;
    }
    if (a1_mode==3) {
        #include "test_const1_restore_inputs.inc"
        a1_mode=a1_mode;
    }
}
void a1_test_assign(int a1_mode, Matrix3d& a, Matrix3d& a1_a, Matrix3d& y, Matrix3d& a1_y) {
    Matrix3d v1f33_0 = Matrix3d::Zero();
    Matrix3d a1_v1f33_0 = Matrix3d::Zero();
    int save_csv1_c=0;
    save_csv1_c=csv1_c;
    if (a1_mode==1) {
        // augmented forward section
        csv1[csv1_c]=0; csv1_c=csv1_c+1;
        v1f33_0=a;
        dsf33[dsf33_c]=y; dsf33_c=dsf33_c+1;
        y=v1f33_0;
        #include "test_assign_store_results.inc"
        // reverse section
        while (csv1_c > save_csv1_c) {
            csv1_c=csv1_c-1;
            if (csv1[csv1_c]==0) {
                dsf33_c=dsf33_c-1; y=dsf33[dsf33_c];
                v1f33_0=a;
                a1_v1f33_0=a1_y; a1_y=Matrix3d::Zero();
                a1_a=a1_a+a1_v1f33_0;
            }
        }
        #include "test_assign_restore_results.inc"
    }
    if (a1_mode==2) {
        #include "test_assign_store_inputs.inc"
        a1_mode=a1_mode;
    }
    if (a1_mode==3) {
        #include "test_assign_restore_inputs.inc"
        a1_mode=a1_mode;
    }
}
void a1_test_paren(int a1_mode, Matrix3d& a, Matrix3d& a1_a, Matrix3d& y, Matrix3d& a1_y) {
    Matrix3d v1f33_0 = Matrix3d::Zero();
    Matrix3d a1_v1f33_0 = Matrix3d::Zero();
    Matrix3d v1f33_1 = Matrix3d::Zero();
    Matrix3d a1_v1f33_1 = Matrix3d::Zero();
    int save_csv1_c=0;
    save_csv1_c=csv1_c;
    if (a1_mode==1) {
        // augmented forward section
        csv1[csv1_c]=0; csv1_c=csv1_c+1;
        v1f33_0=a;
        v1f33_1=v1f33_0;
        dsf33[dsf33_c]=y; dsf33_c=dsf33_c+1;
        y=v1f33_1;
        #include "test_paren_store_results.inc"
        // reverse section
        while (csv1_c > save_csv1_c) {
            csv1_c=csv1_c-1;
            if (csv1[csv1_c]==0) {
                dsf33_c=dsf33_c-1; y=dsf33[dsf33_c];
                v1f33_0=a;
                v1f33_1=v1f33_0;
                a1_v1f33_1=a1_y; a1_y=Matrix3d::Zero();
                a1_v1f33_0=a1_v1f33_1;
                a1_a=a1_a+a1_v1f33_0;
            }
        }
        #include "test_paren_restore_results.inc"
    }
    if (a1_mode==2) {
        #include "test_paren_store_inputs.inc"
        a1_mode=a1_mode;
    }
    if (a1_mode==3) {
        #include "test_paren_restore_inputs.inc"
        a1_mode=a1_mode;
    }
}
void a1_test_uplus(int a1_mode, Matrix3d& a, Matrix3d& a1_a, Matrix3d& y, Matrix3d& a1_y) {
    Matrix3d v1f33_0 = Matrix3d::Zero();
    Matrix3d a1_v1f33_0 = Matrix3d::Zero();
    Matrix3d v1f33_1 = Matrix3d::Zero();
    Matrix3d a1_v1f33_1 = Matrix3d::Zero();
    int save_csv1_c=0;
    save_csv1_c=csv1_c;
    if (a1_mode==1) {
        // augmented forward section
        csv1[csv1_c]=0; csv1_c=csv1_c+1;
        v1f33_0=a;
        v1f33_1=v1f33_0;
        dsf33[dsf33_c]=y; dsf33_c=dsf33_c+1;
        y=v1f33_1;
        #include "test_uplus_store_results.inc"
        // reverse section
        while (csv1_c > save_csv1_c) {
            csv1_c=csv1_c-1;
            if (csv1[csv1_c]==0) {
                dsf33_c=dsf33_c-1; y=dsf33[dsf33_c];
                v1f33_0=a;
                v1f33_1=v1f33_0;
                a1_v1f33_1=a1_y; a1_y=Matrix3d::Zero();
                a1_v1f33_0=a1_v1f33_1;
                a1_a=a1_a+a1_v1f33_0;
            }
        }
        #include "test_uplus_restore_results.inc"
    }
    if (a1_mode==2) {
        #include "test_uplus_store_inputs.inc"
        a1_mode=a1_mode;
    }
    if (a1_mode==3) {
        #include "test_uplus_restore_inputs.inc"
        a1_mode=a1_mode;
    }
}
void a1_test_uminus(int a1_mode, Matrix3d& a, Matrix3d& a1_a, Matrix3d& y, Matrix3d& a1_y) {
    Matrix3d v1f33_0 = Matrix3d::Zero();
    Matrix3d a1_v1f33_0 = Matrix3d::Zero();
    Matrix3d v1f33_1 = Matrix3d::Zero();
    Matrix3d a1_v1f33_1 = Matrix3d::Zero();
    int save_csv1_c=0;
    save_csv1_c=csv1_c;
    if (a1_mode==1) {
        // augmented forward section
        csv1[csv1_c]=0; csv1_c=csv1_c+1;
        v1f33_0=a;
        v1f33_1=-v1f33_0;
        dsf33[dsf33_c]=y; dsf33_c=dsf33_c+1;
        y=v1f33_1;
        #include "test_uminus_store_results.inc"
        // reverse section
        while (csv1_c > save_csv1_c) {
            csv1_c=csv1_c-1;
            if (csv1[csv1_c]==0) {
                dsf33_c=dsf33_c-1; y=dsf33[dsf33_c];
                v1f33_0=a;
                v1f33_1=-v1f33_0;
                a1_v1f33_1=a1_y; a1_y=Matrix3d::Zero();
                a1_v1f33_0=-a1_v1f33_1;
                a1_a=a1_a+a1_v1f33_0;
            }
        }
        #include "test_uminus_restore_results.inc"
    }
    if (a1_mode==2) {
        #include "test_uminus_store_inputs.inc"
        a1_mode=a1_mode;
    }
    if (a1_mode==3) {
        #include "test_uminus_restore_inputs.inc"
        a1_mode=a1_mode;
    }
}
void a1_test_eval(int a1_mode, Matrix<double,2,3>& a, Matrix<double,2,3>& a1_a, Matrix<double,2,3>& y, Matrix<double,2,3>& a1_y) {
    Matrix<double,2,3> v1f23_0 = Matrix<double,2,3>::Zero();
    Matrix<double,2,3> a1_v1f23_0 = Matrix<double,2,3>::Zero();
    Matrix<double,2,3> v1f23_1 = Matrix<double,2,3>::Zero();
    Matrix<double,2,3> a1_v1f23_1 = Matrix<double,2,3>::Zero();
    int save_csv1_c=0;
    save_csv1_c=csv1_c;
    if (a1_mode==1) {
        // augmented forward section
        csv1[csv1_c]=0; csv1_c=csv1_c+1;
        v1f23_0=a;
        v1f23_1=v1f23_0;
        dsf23[dsf23_c]=y; dsf23_c=dsf23_c+1;
        y=v1f23_1;
        #include "test_eval_store_results.inc"
        // reverse section
        while (csv1_c > save_csv1_c) {
            csv1_c=csv1_c-1;
            if (csv1[csv1_c]==0) {
                dsf23_c=dsf23_c-1; y=dsf23[dsf23_c];
                v1f23_0=a;
                v1f23_1=v1f23_0;
                a1_v1f23_1=a1_y; a1_y=Matrix<double,2,3>::Zero();
                a1_v1f23_0=a1_v1f23_1;
                a1_a=a1_a+a1_v1f23_0;
            }
        }
        #include "test_eval_restore_results.inc"
    }
    if (a1_mode==2) {
        #include "test_eval_store_inputs.inc"
        a1_mode=a1_mode;
    }
    if (a1_mode==3) {
        #include "test_eval_restore_inputs.inc"
        a1_mode=a1_mode;
    }
}
void a1_test_transpose(int a1_mode, Matrix<double,2,3>& a, Matrix<double,2,3>& a1_a, Matrix<double,3,2>& y, Matrix<double,3,2>& a1_y) {
    Matrix<double,2,3> v1f23_0 = Matrix<double,2,3>::Zero();
    Matrix<double,2,3> a1_v1f23_0 = Matrix<double,2,3>::Zero();
    Matrix<double,3,2> v1f32_1 = Matrix<double,3,2>::Zero();
    Matrix<double,3,2> a1_v1f32_1 = Matrix<double,3,2>::Zero();
    int save_csv1_c=0;
    save_csv1_c=csv1_c;
    if (a1_mode==1) {
        // augmented forward section
        csv1[csv1_c]=0; csv1_c=csv1_c+1;
        v1f23_0=a;
        v1f32_1=v1f23_0.transpose();
        dsf32[dsf32_c]=y; dsf32_c=dsf32_c+1;
        y=v1f32_1;
        #include "test_transpose_store_results.inc"
        // reverse section
        while (csv1_c > save_csv1_c) {
            csv1_c=csv1_c-1;
            if (csv1[csv1_c]==0) {
                dsf32_c=dsf32_c-1; y=dsf32[dsf32_c];
                v1f23_0=a;
                v1f32_1=v1f23_0.transpose();
                a1_v1f32_1=a1_y; a1_y=Matrix<double,3,2>::Zero();
                a1_v1f23_0=a1_v1f32_1.transpose();
                a1_a=a1_a+a1_v1f23_0;
            }
        }
        #include "test_transpose_restore_results.inc"
    }
    if (a1_mode==2) {
        #include "test_transpose_store_inputs.inc"
        a1_mode=a1_mode;
    }
    if (a1_mode==3) {
        #include "test_transpose_restore_inputs.inc"
        a1_mode=a1_mode;
    }
}
void a1_test_inverse(int a1_mode, Matrix3d& a, Matrix3d& a1_a, Matrix3d& y, Matrix3d& a1_y) {
    Matrix3d v1f33_0 = Matrix3d::Zero();
    Matrix3d a1_v1f33_0 = Matrix3d::Zero();
    Matrix3d v1f33_1 = Matrix3d::Zero();
    Matrix3d a1_v1f33_1 = Matrix3d::Zero();
    int save_csv1_c=0;
    save_csv1_c=csv1_c;
    if (a1_mode==1) {
        // augmented forward section
        csv1[csv1_c]=0; csv1_c=csv1_c+1;
        v1f33_0=a;
        v1f33_1=v1f33_0.inverse();
        dsf33[dsf33_c]=v1f33_1; dsf33_c=dsf33_c+1;
        dsf33[dsf33_c]=y; dsf33_c=dsf33_c+1;
        y=v1f33_1;
        #include "test_inverse_store_results.inc"
        // reverse section
        while (csv1_c > save_csv1_c) {
            csv1_c=csv1_c-1;
            if (csv1[csv1_c]==0) {
                dsf33_c=dsf33_c-1; y=dsf33[dsf33_c];
                v1f33_0=a;
                dsf33_c=dsf33_c-1; v1f33_1=dsf33[dsf33_c];
                a1_v1f33_1=a1_y; a1_y=Matrix3d::Zero();
                a1_v1f33_0=-v1f33_1.transpose()*a1_v1f33_1*v1f33_1.transpose();
                a1_a=a1_a+a1_v1f33_0;
            }
        }
        #include "test_inverse_restore_results.inc"
    }
    if (a1_mode==2) {
        #include "test_inverse_store_inputs.inc"
        a1_mode=a1_mode;
    }
    if (a1_mode==3) {
        #include "test_inverse_restore_inputs.inc"
        a1_mode=a1_mode;
    }
}
void a1_test_trace(int a1_mode, Matrix3d& a, Matrix3d& a1_a, double& y, double& a1_y) {
    Matrix3d v1f33_0 = Matrix3d::Zero();
    Matrix3d a1_v1f33_0 = Matrix3d::Zero();
    double v1f_1 = 0;
    double a1_v1f_1 = 0;
    int save_csv1_c=0;
    save_csv1_c=csv1_c;
    if (a1_mode==1) {
        // augmented forward section
        csv1[csv1_c]=0; csv1_c=csv1_c+1;
        v1f33_0=a;
        v1f_1=v1f33_0.trace();
        dsf[dsf_c]=y; dsf_c=dsf_c+1;
        y=v1f_1;
        #include "test_trace_store_results.inc"
        // reverse section
        while (csv1_c > save_csv1_c) {
            csv1_c=csv1_c-1;
            if (csv1[csv1_c]==0) {
                dsf_c=dsf_c-1; y=dsf[dsf_c];
                v1f33_0=a;
                v1f_1=v1f33_0.trace();
                a1_v1f_1=a1_y; a1_y=0;
                a1_v1f33_0=a1_v1f_1*Matrix3d::Identity();
                a1_a=a1_a+a1_v1f33_0;
            }
        }
        #include "test_trace_restore_results.inc"
    }
    if (a1_mode==2) {
        #include "test_trace_store_inputs.inc"
        a1_mode=a1_mode;
    }
    if (a1_mode==3) {
        #include "test_trace_restore_inputs.inc"
        a1_mode=a1_mode;
    }
}
void a1_test_determinant(int a1_mode, Matrix3d& a, Matrix3d& a1_a, double& y, double& a1_y) {
    Matrix3d v1f33_0 = Matrix3d::Zero();
    Matrix3d a1_v1f33_0 = Matrix3d::Zero();
    double v1f_1 = 0;
    double a1_v1f_1 = 0;
    int save_csv1_c=0;
    save_csv1_c=csv1_c;
    if (a1_mode==1) {
        // augmented forward section
        csv1[csv1_c]=0; csv1_c=csv1_c+1;
        v1f33_0=a;
        v1f_1=v1f33_0.determinant();
        dsf[dsf_c]=v1f_1; dsf_c=dsf_c+1;
        dsf[dsf_c]=y; dsf_c=dsf_c+1;
        y=v1f_1;
        #include "test_determinant_store_results.inc"
        // reverse section
        while (csv1_c > save_csv1_c) {
            csv1_c=csv1_c-1;
            if (csv1[csv1_c]==0) {
                dsf_c=dsf_c-1; y=dsf[dsf_c];
                v1f33_0=a;
                dsf_c=dsf_c-1; v1f_1=dsf[dsf_c];
                a1_v1f_1=a1_y; a1_y=0;
                a1_v1f33_0=a1_v1f_1*v1f_1*v1f33_0.transpose().inverse();
                a1_a=a1_a+a1_v1f33_0;
            }
        }
        #include "test_determinant_restore_results.inc"
    }
    if (a1_mode==2) {
        #include "test_determinant_store_inputs.inc"
        a1_mode=a1_mode;
    }
    if (a1_mode==3) {
        #include "test_determinant_restore_inputs.inc"
        a1_mode=a1_mode;
    }
}
void a1_test_add(int a1_mode, Matrix3d& a, Matrix3d& a1_a, Matrix3d& b, Matrix3d& a1_b, Matrix3d& y, Matrix3d& a1_y) {
    Matrix3d v1f33_0 = Matrix3d::Zero();
    Matrix3d a1_v1f33_0 = Matrix3d::Zero();
    Matrix3d v1f33_1 = Matrix3d::Zero();
    Matrix3d a1_v1f33_1 = Matrix3d::Zero();
    Matrix3d v1f33_2 = Matrix3d::Zero();
    Matrix3d a1_v1f33_2 = Matrix3d::Zero();
    int save_csv1_c=0;
    save_csv1_c=csv1_c;
    if (a1_mode==1) {
        // augmented forward section
        csv1[csv1_c]=0; csv1_c=csv1_c+1;
        v1f33_0=a;
        v1f33_1=b;
        v1f33_2=v1f33_0+v1f33_1;
        dsf33[dsf33_c]=y; dsf33_c=dsf33_c+1;
        y=v1f33_2;
        #include "test_add_store_results.inc"
        // reverse section
        while (csv1_c > save_csv1_c) {
            csv1_c=csv1_c-1;
            if (csv1[csv1_c]==0) {
                dsf33_c=dsf33_c-1; y=dsf33[dsf33_c];
                v1f33_0=a;
                v1f33_1=b;
                v1f33_2=v1f33_0+v1f33_1;
                a1_v1f33_2=a1_y; a1_y=Matrix3d::Zero();
                a1_v1f33_0=a1_v1f33_2; a1_v1f33_1=a1_v1f33_2;
                a1_b=a1_b+a1_v1f33_1;
                a1_a=a1_a+a1_v1f33_0;
            }
        }
        #include "test_add_restore_results.inc"
    }
    if (a1_mode==2) {
        #include "test_add_store_inputs.inc"
        a1_mode=a1_mode;
    }
    if (a1_mode==3) {
        #include "test_add_restore_inputs.inc"
        a1_mode=a1_mode;
    }
}
void a1_test_sub(int a1_mode, Matrix3d& a, Matrix3d& a1_a, Matrix3d& b, Matrix3d& a1_b, Matrix3d& y, Matrix3d& a1_y) {
    Matrix3d v1f33_0 = Matrix3d::Zero();
    Matrix3d a1_v1f33_0 = Matrix3d::Zero();
    Matrix3d v1f33_1 = Matrix3d::Zero();
    Matrix3d a1_v1f33_1 = Matrix3d::Zero();
    Matrix3d v1f33_2 = Matrix3d::Zero();
    Matrix3d a1_v1f33_2 = Matrix3d::Zero();
    int save_csv1_c=0;
    save_csv1_c=csv1_c;
    if (a1_mode==1) {
        // augmented forward section
        csv1[csv1_c]=0; csv1_c=csv1_c+1;
        v1f33_0=a;
        v1f33_1=b;
        v1f33_2=v1f33_0-v1f33_1;
        dsf33[dsf33_c]=y; dsf33_c=dsf33_c+1;
        y=v1f33_2;
        #include "test_sub_store_results.inc"
        // reverse section
        while (csv1_c > save_csv1_c) {
            csv1_c=csv1_c-1;
            if (csv1[csv1_c]==0) {
                dsf33_c=dsf33_c-1; y=dsf33[dsf33_c];
                v1f33_0=a;
                v1f33_1=b;
                v1f33_2=v1f33_0-v1f33_1;
                a1_v1f33_2=a1_y; a1_y=Matrix3d::Zero();
                a1_v1f33_0=a1_v1f33_2; a1_v1f33_1=-a1_v1f33_2;
                a1_b=a1_b+a1_v1f33_1;
                a1_a=a1_a+a1_v1f33_0;
            }
        }
        #include "test_sub_restore_results.inc"
    }
    if (a1_mode==2) {
        #include "test_sub_store_inputs.inc"
        a1_mode=a1_mode;
    }
    if (a1_mode==3) {
        #include "test_sub_restore_inputs.inc"
        a1_mode=a1_mode;
    }
}
void a1_test_mult(int a1_mode, Matrix<double,2,3>& a, Matrix<double,2,3>& a1_a, Matrix<double,3,4>& b, Matrix<double,3,4>& a1_b, Matrix<double,2,4>& y, Matrix<double,2,4>& a1_y) {
    Matrix<double,2,3> v1f23_0 = Matrix<double,2,3>::Zero();
    Matrix<double,2,3> a1_v1f23_0 = Matrix<double,2,3>::Zero();
    Matrix<double,3,4> v1f34_1 = Matrix<double,3,4>::Zero();
    Matrix<double,3,4> a1_v1f34_1 = Matrix<double,3,4>::Zero();
    Matrix<double,2,4> v1f24_2 = Matrix<double,2,4>::Zero();
    Matrix<double,2,4> a1_v1f24_2 = Matrix<double,2,4>::Zero();
    int save_csv1_c=0;
    save_csv1_c=csv1_c;
    if (a1_mode==1) {
        // augmented forward section
        csv1[csv1_c]=0; csv1_c=csv1_c+1;
        v1f23_0=a;
        v1f34_1=b;
        v1f24_2=v1f23_0*v1f34_1;
        dsf24[dsf24_c]=y; dsf24_c=dsf24_c+1;
        y=v1f24_2;
        #include "test_mult_store_results.inc"
        // reverse section
        while (csv1_c > save_csv1_c) {
            csv1_c=csv1_c-1;
            if (csv1[csv1_c]==0) {
                dsf24_c=dsf24_c-1; y=dsf24[dsf24_c];
                v1f23_0=a;
                v1f34_1=b;
                v1f24_2=v1f23_0*v1f34_1;
                a1_v1f24_2=a1_y; a1_y=Matrix<double,2,4>::Zero();
                a1_v1f23_0=a1_v1f24_2*v1f34_1.transpose(); a1_v1f34_1=v1f23_0.transpose()*a1_v1f24_2;
                a1_b=a1_b+a1_v1f34_1;
                a1_a=a1_a+a1_v1f23_0;
            }
        }
        #include "test_mult_restore_results.inc"
    }
    if (a1_mode==2) {
        #include "test_mult_store_inputs.inc"
        a1_mode=a1_mode;
    }
    if (a1_mode==3) {
        #include "test_mult_restore_inputs.inc"
        a1_mode=a1_mode;
    }
}
void a1_test_scalar_mult1(int a1_mode, double a, double a1_a, Matrix<double,2,3>& b, Matrix<double,2,3>& a1_b, Matrix<double,2,3>& y, Matrix<double,2,3>& a1_y) {
    double v1f_0 = 0;
    double a1_v1f_0 = 0;
    Matrix<double,2,3> v1f23_1 = Matrix<double,2,3>::Zero();
    Matrix<double,2,3> a1_v1f23_1 = Matrix<double,2,3>::Zero();
    Matrix<double,2,3> v1f23_2 = Matrix<double,2,3>::Zero();
    Matrix<double,2,3> a1_v1f23_2 = Matrix<double,2,3>::Zero();
    int save_csv1_c=0;
    save_csv1_c=csv1_c;
    if (a1_mode==1) {
        // augmented forward section
        csv1[csv1_c]=0; csv1_c=csv1_c+1;
        v1f_0=a;
        v1f23_1=b;
        v1f23_2=v1f_0*v1f23_1;
        dsf23[dsf23_c]=y; dsf23_c=dsf23_c+1;
        y=v1f23_2;
        #include "test_scalar_mult1_store_results.inc"
        // reverse section
        while (csv1_c > save_csv1_c) {
            csv1_c=csv1_c-1;
            if (csv1[csv1_c]==0) {
                dsf23_c=dsf23_c-1; y=dsf23[dsf23_c];
                v1f_0=a;
                v1f23_1=b;
                v1f23_2=v1f_0*v1f23_1;
                a1_v1f23_2=a1_y; a1_y=Matrix<double,2,3>::Zero();
                a1_v1f_0=(a1_v1f23_2*v1f23_1.transpose()).trace(); a1_v1f23_1=v1f_0*a1_v1f23_2;
                a1_b=a1_b+a1_v1f23_1;
                a1_a=a1_a+a1_v1f_0;
            }
        }
        #include "test_scalar_mult1_restore_results.inc"
    }
    if (a1_mode==2) {
        #include "test_scalar_mult1_store_inputs.inc"
        a1_mode=a1_mode;
    }
    if (a1_mode==3) {
        #include "test_scalar_mult1_restore_inputs.inc"
        a1_mode=a1_mode;
    }
}
void a1_test_scalar_mult2(int a1_mode, Matrix<double,2,3>& a, Matrix<double,2,3>& a1_a, double b, double a1_b, Matrix<double,2,3>& y, Matrix<double,2,3>& a1_y) {
    Matrix<double,2,3> v1f23_0 = Matrix<double,2,3>::Zero();
    Matrix<double,2,3> a1_v1f23_0 = Matrix<double,2,3>::Zero();
    double v1f_1 = 0;
    double a1_v1f_1 = 0;
    Matrix<double,2,3> v1f23_2 = Matrix<double,2,3>::Zero();
    Matrix<double,2,3> a1_v1f23_2 = Matrix<double,2,3>::Zero();
    int save_csv1_c=0;
    save_csv1_c=csv1_c;
    if (a1_mode==1) {
        // augmented forward section
        csv1[csv1_c]=0; csv1_c=csv1_c+1;
        v1f23_0=a;
        v1f_1=b;
        v1f23_2=v1f23_0*v1f_1;
        dsf23[dsf23_c]=y; dsf23_c=dsf23_c+1;
        y=v1f23_2;
        #include "test_scalar_mult2_store_results.inc"
        // reverse section
        while (csv1_c > save_csv1_c) {
            csv1_c=csv1_c-1;
            if (csv1[csv1_c]==0) {
                dsf23_c=dsf23_c-1; y=dsf23[dsf23_c];
                v1f23_0=a;
                v1f_1=b;
                v1f23_2=v1f23_0*v1f_1;
                a1_v1f23_2=a1_y; a1_y=Matrix<double,2,3>::Zero();
                a1_v1f23_0=a1_v1f23_2*v1f_1; a1_v1f_1=(v1f23_0.transpose()*a1_v1f23_2).trace();
                a1_b=a1_b+a1_v1f_1;
                a1_a=a1_a+a1_v1f23_0;
            }
        }
        #include "test_scalar_mult2_restore_results.inc"
    }
    if (a1_mode==2) {
        #include "test_scalar_mult2_store_inputs.inc"
        a1_mode=a1_mode;
    }
    if (a1_mode==3) {
        #include "test_scalar_mult2_restore_inputs.inc"
        a1_mode=a1_mode;
    }
}
void a1_test_scalar_div(int a1_mode, Matrix<double,2,3>& a, Matrix<double,2,3>& a1_a, double b, double a1_b, Matrix<double,2,3>& y, Matrix<double,2,3>& a1_y) {
    Matrix<double,2,3> v1f23_0 = Matrix<double,2,3>::Zero();
    Matrix<double,2,3> a1_v1f23_0 = Matrix<double,2,3>::Zero();
    double v1f_1 = 0;
    double a1_v1f_1 = 0;
    Matrix<double,2,3> v1f23_2 = Matrix<double,2,3>::Zero();
    Matrix<double,2,3> a1_v1f23_2 = Matrix<double,2,3>::Zero();
    int save_csv1_c=0;
    save_csv1_c=csv1_c;
    if (a1_mode==1) {
        // augmented forward section
        csv1[csv1_c]=0; csv1_c=csv1_c+1;
        v1f23_0=a;
        v1f_1=b;
        v1f23_2=v1f23_0/v1f_1;
        dsf23[dsf23_c]=y; dsf23_c=dsf23_c+1;
        y=v1f23_2;
        #include "test_scalar_div_store_results.inc"
        // reverse section
        while (csv1_c > save_csv1_c) {
            csv1_c=csv1_c-1;
            if (csv1[csv1_c]==0) {
                dsf23_c=dsf23_c-1; y=dsf23[dsf23_c];
                v1f23_0=a;
                v1f_1=b;
                v1f23_2=v1f23_0/v1f_1;
                a1_v1f23_2=a1_y; a1_y=Matrix<double,2,3>::Zero();
                a1_v1f23_0=a1_v1f23_2/v1f_1; a1_v1f_1=-(v1f23_0.transpose()*a1_v1f23_2).trace()/(v1f_1*v1f_1);
                a1_b=a1_b+a1_v1f_1;
                a1_a=a1_a+a1_v1f23_0;
            }
        }
        #include "test_scalar_div_restore_results.inc"
    }
    if (a1_mode==2) {
        #include "test_scalar_div_store_inputs.inc"
        a1_mode=a1_mode;
    }
    if (a1_mode==3) {
        #include "test_scalar_div_restore_inputs.inc"
        a1_mode=a1_mode;
    }
}
void a1_test_dot(int a1_mode, Vector3d& a, Vector3d& a1_a, Vector3d& b, Vector3d& a1_b, double& y, double& a1_y) {
    Vector3d v1f31_0 = Vector3d::Zero();
    Vector3d a1_v1f31_0 = Vector3d::Zero();
    Vector3d v1f31_1 = Vector3d::Zero();
    Vector3d a1_v1f31_1 = Vector3d::Zero();
    double v1f_2 = 0;
    double a1_v1f_2 = 0;
    int save_csv1_c=0;
    save_csv1_c=csv1_c;
    if (a1_mode==1) {
        // augmented forward section
        csv1[csv1_c]=0; csv1_c=csv1_c+1;
        v1f31_0=a;
        v1f31_1=b;
        v1f_2=v1f31_0.dot(v1f31_1);
        dsf[dsf_c]=y; dsf_c=dsf_c+1;
        y=v1f_2;
        #include "test_dot_store_results.inc"
        // reverse section
        while (csv1_c > save_csv1_c) {
            csv1_c=csv1_c-1;
            if (csv1[csv1_c]==0) {
                dsf_c=dsf_c-1; y=dsf[dsf_c];
                v1f31_0=a;
                v1f31_1=b;
                v1f_2=v1f31_0.dot(v1f31_1);
                a1_v1f_2=a1_y; a1_y=0;
                a1_v1f31_0=a1_v1f_2*v1f31_1; a1_v1f31_1=a1_v1f_2*v1f31_0;
                a1_b=a1_b+a1_v1f31_1;
                a1_a=a1_a+a1_v1f31_0;
            }
        }
        #include "test_dot_restore_results.inc"
    }
    if (a1_mode==2) {
        #include "test_dot_store_inputs.inc"
        a1_mode=a1_mode;
    }
    if (a1_mode==3) {
        #include "test_dot_restore_inputs.inc"
        a1_mode=a1_mode;
    }
}
void a1_test_solve(int a1_mode, Matrix<double,2,3>& a, Matrix<double,2,3>& a1_a, Vector2d& b, Vector2d& a1_b, Vector3d& x, Vector3d& a1_x) {
    Matrix<double,2,3> v1f23_0 = Matrix<double,2,3>::Zero();
    Matrix<double,2,3> a1_v1f23_0 = Matrix<double,2,3>::Zero();
    PartialPivLU<Matrix<double,2,3>> v1f23d1_1 = PartialPivLU<Matrix<double,2,3>>();
    Matrix<double,2,3> a1_v1f23d1_1 = Matrix<double,2,3>::Zero();
    Vector2d v1f21_2 = Vector2d::Zero();
    Vector2d a1_v1f21_2 = Vector2d::Zero();
    Vector3d v1f31_3 = Vector3d::Zero();
    Vector3d a1_v1f31_3 = Vector3d::Zero();
    int save_csv1_c=0;
    save_csv1_c=csv1_c;
    if (a1_mode==1) {
        // augmented forward section
        csv1[csv1_c]=0; csv1_c=csv1_c+1;
        v1f23_0=a;
        v1f23d1_1=v1f23_0.partialPivLu();
        v1f21_2=b;
        v1f31_3=v1f23d1_1.solve(v1f21_2);
        dsf31[dsf31_c]=v1f31_3; dsf31_c=dsf31_c+1;
        dsf23d1[dsf23d1_c]=v1f23d1_1; dsf23d1_c=dsf23d1_c+1;
        dsf31[dsf31_c]=x; dsf31_c=dsf31_c+1;
        x=v1f31_3;
        #include "test_solve_store_results.inc"
        // reverse section
        while (csv1_c > save_csv1_c) {
            csv1_c=csv1_c-1;
            if (csv1[csv1_c]==0) {
                dsf31_c=dsf31_c-1; x=dsf31[dsf31_c];
                v1f23_0=a;
                dsf23d1_c=dsf23d1_c-1; v1f23d1_1=dsf23d1[dsf23d1_c];
                v1f21_2=b;
                dsf31_c=dsf31_c-1; v1f31_3=dsf31[dsf31_c];
                a1_v1f31_3=a1_x; a1_x=Vector3d::Zero();
                a1_v1f21_2=v1f23d1_1.transpose().solve(a1_v1f31_3); a1_v1f23d1_1=-a1_v1f21_2*v1f31_3.transpose();
                a1_b=a1_b+a1_v1f21_2;
                a1_v1f23_0=a1_v1f23d1_1;
                a1_a=a1_a+a1_v1f23_0;
            }
        }
        #include "test_solve_restore_results.inc"
    }
    if (a1_mode==2) {
        #include "test_solve_store_inputs.inc"
        a1_mode=a1_mode;
    }
    if (a1_mode==3) {
        #include "test_solve_restore_inputs.inc"
        a1_mode=a1_mode;
    }
}
void a1_test_solve_transposed(int a1_mode, Matrix<double,3,2>& a, Matrix<double,3,2>& a1_a, Vector2d& b, Vector2d& a1_b, Vector3d& x, Vector3d& a1_x) {
    Matrix<double,3,2> v1f32_0 = Matrix<double,3,2>::Zero();
    Matrix<double,3,2> a1_v1f32_0 = Matrix<double,3,2>::Zero();
    PartialPivLU<Matrix<double,3,2>> v1f32d1_1 = PartialPivLU<Matrix<double,3,2>>();
    Matrix<double,3,2> a1_v1f32d1_1 = Matrix<double,3,2>::Zero();
    Vector2d v1f21_2 = Vector2d::Zero();
    Vector2d a1_v1f21_2 = Vector2d::Zero();
    Vector3d v1f31_3 = Vector3d::Zero();
    Vector3d a1_v1f31_3 = Vector3d::Zero();
    int save_csv1_c=0;
    save_csv1_c=csv1_c;
    if (a1_mode==1) {
        // augmented forward section
        csv1[csv1_c]=0; csv1_c=csv1_c+1;
        v1f32_0=a;
        v1f32d1_1=v1f32_0.partialPivLu();
        v1f21_2=b;
        v1f31_3=v1f32d1_1.transpose().solve(v1f21_2);
        dsf31[dsf31_c]=v1f31_3; dsf31_c=dsf31_c+1;
        dsf32d1[dsf32d1_c]=v1f32d1_1; dsf32d1_c=dsf32d1_c+1;
        dsf31[dsf31_c]=x; dsf31_c=dsf31_c+1;
        x=v1f31_3;
        #include "test_solve_transposed_store_results.inc"
        // reverse section
        while (csv1_c > save_csv1_c) {
            csv1_c=csv1_c-1;
            if (csv1[csv1_c]==0) {
                dsf31_c=dsf31_c-1; x=dsf31[dsf31_c];
                v1f32_0=a;
                dsf32d1_c=dsf32d1_c-1; v1f32d1_1=dsf32d1[dsf32d1_c];
                v1f21_2=b;
                dsf31_c=dsf31_c-1; v1f31_3=dsf31[dsf31_c];
                a1_v1f31_3=a1_x; a1_x=Vector3d::Zero();
                a1_v1f21_2=v1f32d1_1.solve(a1_v1f31_3); a1_v1f32d1_1=-a1_v1f21_2*v1f31_3.transpose();
                a1_b=a1_b+a1_v1f21_2;
                a1_v1f32_0=a1_v1f32d1_1;
                a1_a=a1_a+a1_v1f32_0;
            }
        }
        #include "test_solve_transposed_restore_results.inc"
    }
    if (a1_mode==2) {
        #include "test_solve_transposed_store_inputs.inc"
        a1_mode=a1_mode;
    }
    if (a1_mode==3) {
        #include "test_solve_transposed_restore_inputs.inc"
        a1_mode=a1_mode;
    }
}
