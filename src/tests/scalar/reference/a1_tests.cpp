int csv1[1000000]; int csv1_c = 0;
double dsf[100000]; int dsf_c = 0;
#include "declare_checkpoints.inc"
#include "tests.cpp"
void a1_test_const0(int a1_mode, double& y, double& a1_y) {
    double v1f_0 = 0;
    double a1_v1f_0 = 0;
    int save_csv1_c=0;
    save_csv1_c=csv1_c;
    if (a1_mode==1) {
        // augmented forward section
        csv1[csv1_c]=0; csv1_c=csv1_c+1;
        v1f_0=0;
        dsf[dsf_c]=y; dsf_c=dsf_c+1;
        y=v1f_0;
        #include "test_const0_store_results.inc"
        // reverse section
        while (csv1_c > save_csv1_c) {
            csv1_c=csv1_c-1;
            if (csv1[csv1_c]==0) {
                dsf_c=dsf_c-1; y=dsf[dsf_c];
                v1f_0=0;
                a1_y=0;
            }
        }
        #include "test_const0_restore_results.inc"
    }
    if (a1_mode==2) {
        #include "test_const0_store_inputs.inc"
        a1_mode=a1_mode;
    }
    if (a1_mode==3) {
        #include "test_const0_restore_inputs.inc"
        a1_mode=a1_mode;
    }
}
void a1_test_const1(int a1_mode, double& y, double& a1_y) {
    double v1f_0 = 0;
    double a1_v1f_0 = 0;
    int save_csv1_c=0;
    save_csv1_c=csv1_c;
    if (a1_mode==1) {
        // augmented forward section
        csv1[csv1_c]=0; csv1_c=csv1_c+1;
        v1f_0=42;
        dsf[dsf_c]=y; dsf_c=dsf_c+1;
        y=v1f_0;
        #include "test_const1_store_results.inc"
        // reverse section
        while (csv1_c > save_csv1_c) {
            csv1_c=csv1_c-1;
            if (csv1[csv1_c]==0) {
                dsf_c=dsf_c-1; y=dsf[dsf_c];
                v1f_0=42;
                a1_y=0;
            }
        }
        #include "test_const1_restore_results.inc"
    }
    if (a1_mode==2) {
        #include "test_const1_store_inputs.inc"
        a1_mode=a1_mode;
    }
    if (a1_mode==3) {
        #include "test_const1_restore_inputs.inc"
        a1_mode=a1_mode;
    }
}
void a1_test_assign(int a1_mode, double x, double a1_x, double& y, double& a1_y) {
    double v1f_0 = 0;
    double a1_v1f_0 = 0;
    int save_csv1_c=0;
    save_csv1_c=csv1_c;
    if (a1_mode==1) {
        // augmented forward section
        csv1[csv1_c]=0; csv1_c=csv1_c+1;
        v1f_0=x;
        dsf[dsf_c]=y; dsf_c=dsf_c+1;
        y=v1f_0;
        #include "test_assign_store_results.inc"
        // reverse section
        while (csv1_c > save_csv1_c) {
            csv1_c=csv1_c-1;
            if (csv1[csv1_c]==0) {
                dsf_c=dsf_c-1; y=dsf[dsf_c];
                v1f_0=x;
                a1_v1f_0=a1_y; a1_y=0;
                a1_x=a1_x+a1_v1f_0;
            }
        }
        #include "test_assign_restore_results.inc"
    }
    if (a1_mode==2) {
        #include "test_assign_store_inputs.inc"
        a1_mode=a1_mode;
    }
    if (a1_mode==3) {
        #include "test_assign_restore_inputs.inc"
        a1_mode=a1_mode;
    }
}
void a1_test_paren(int a1_mode, double x, double a1_x, double& y, double& a1_y) {
    double v1f_0 = 0;
    double a1_v1f_0 = 0;
    double v1f_1 = 0;
    double a1_v1f_1 = 0;
    int save_csv1_c=0;
    save_csv1_c=csv1_c;
    if (a1_mode==1) {
        // augmented forward section
        csv1[csv1_c]=0; csv1_c=csv1_c+1;
        v1f_0=x;
        v1f_1=v1f_0;
        dsf[dsf_c]=y; dsf_c=dsf_c+1;
        y=v1f_1;
        #include "test_paren_store_results.inc"
        // reverse section
        while (csv1_c > save_csv1_c) {
            csv1_c=csv1_c-1;
            if (csv1[csv1_c]==0) {
                dsf_c=dsf_c-1; y=dsf[dsf_c];
                v1f_0=x;
                v1f_1=v1f_0;
                a1_v1f_1=a1_y; a1_y=0;
                a1_v1f_0=a1_v1f_1;
                a1_x=a1_x+a1_v1f_0;
            }
        }
        #include "test_paren_restore_results.inc"
    }
    if (a1_mode==2) {
        #include "test_paren_store_inputs.inc"
        a1_mode=a1_mode;
    }
    if (a1_mode==3) {
        #include "test_paren_restore_inputs.inc"
        a1_mode=a1_mode;
    }
}
void a1_test_uplus(int a1_mode, double x, double a1_x, double& y, double& a1_y) {
    double v1f_0 = 0;
    double a1_v1f_0 = 0;
    double v1f_1 = 0;
    double a1_v1f_1 = 0;
    int save_csv1_c=0;
    save_csv1_c=csv1_c;
    if (a1_mode==1) {
        // augmented forward section
        csv1[csv1_c]=0; csv1_c=csv1_c+1;
        v1f_0=x;
        v1f_1=v1f_0;
        dsf[dsf_c]=y; dsf_c=dsf_c+1;
        y=v1f_1;
        #include "test_uplus_store_results.inc"
        // reverse section
        while (csv1_c > save_csv1_c) {
            csv1_c=csv1_c-1;
            if (csv1[csv1_c]==0) {
                dsf_c=dsf_c-1; y=dsf[dsf_c];
                v1f_0=x;
                v1f_1=v1f_0;
                a1_v1f_1=a1_y; a1_y=0;
                a1_v1f_0=a1_v1f_1;
                a1_x=a1_x+a1_v1f_0;
            }
        }
        #include "test_uplus_restore_results.inc"
    }
    if (a1_mode==2) {
        #include "test_uplus_store_inputs.inc"
        a1_mode=a1_mode;
    }
    if (a1_mode==3) {
        #include "test_uplus_restore_inputs.inc"
        a1_mode=a1_mode;
    }
}
void a1_test_uminus(int a1_mode, double x, double a1_x, double& y, double& a1_y) {
    double v1f_0 = 0;
    double a1_v1f_0 = 0;
    double v1f_1 = 0;
    double a1_v1f_1 = 0;
    int save_csv1_c=0;
    save_csv1_c=csv1_c;
    if (a1_mode==1) {
        // augmented forward section
        csv1[csv1_c]=0; csv1_c=csv1_c+1;
        v1f_0=x;
        v1f_1=-v1f_0;
        dsf[dsf_c]=y; dsf_c=dsf_c+1;
        y=v1f_1;
        #include "test_uminus_store_results.inc"
        // reverse section
        while (csv1_c > save_csv1_c) {
            csv1_c=csv1_c-1;
            if (csv1[csv1_c]==0) {
                dsf_c=dsf_c-1; y=dsf[dsf_c];
                v1f_0=x;
                v1f_1=-v1f_0;
                a1_v1f_1=a1_y; a1_y=0;
                a1_v1f_0=-a1_v1f_1;
                a1_x=a1_x+a1_v1f_0;
            }
        }
        #include "test_uminus_restore_results.inc"
    }
    if (a1_mode==2) {
        #include "test_uminus_store_inputs.inc"
        a1_mode=a1_mode;
    }
    if (a1_mode==3) {
        #include "test_uminus_restore_inputs.inc"
        a1_mode=a1_mode;
    }
}
void a1_test_sin(int a1_mode, double x, double a1_x, double& y, double& a1_y) {
    double v1f_0 = 0;
    double a1_v1f_0 = 0;
    double v1f_1 = 0;
    double a1_v1f_1 = 0;
    int save_csv1_c=0;
    save_csv1_c=csv1_c;
    if (a1_mode==1) {
        // augmented forward section
        csv1[csv1_c]=0; csv1_c=csv1_c+1;
        v1f_0=x;
        v1f_1=sin(v1f_0);
        dsf[dsf_c]=y; dsf_c=dsf_c+1;
        y=v1f_1;
        #include "test_sin_store_results.inc"
        // reverse section
        while (csv1_c > save_csv1_c) {
            csv1_c=csv1_c-1;
            if (csv1[csv1_c]==0) {
                dsf_c=dsf_c-1; y=dsf[dsf_c];
                v1f_0=x;
                v1f_1=sin(v1f_0);
                a1_v1f_1=a1_y; a1_y=0;
                a1_v1f_0=cos(v1f_0)*a1_v1f_1;
                a1_x=a1_x+a1_v1f_0;
            }
        }
        #include "test_sin_restore_results.inc"
    }
    if (a1_mode==2) {
        #include "test_sin_store_inputs.inc"
        a1_mode=a1_mode;
    }
    if (a1_mode==3) {
        #include "test_sin_restore_inputs.inc"
        a1_mode=a1_mode;
    }
}
void a1_test_cos(int a1_mode, double x, double a1_x, double& y, double& a1_y) {
    double v1f_0 = 0;
    double a1_v1f_0 = 0;
    double v1f_1 = 0;
    double a1_v1f_1 = 0;
    int save_csv1_c=0;
    save_csv1_c=csv1_c;
    if (a1_mode==1) {
        // augmented forward section
        csv1[csv1_c]=0; csv1_c=csv1_c+1;
        v1f_0=x;
        v1f_1=cos(v1f_0);
        dsf[dsf_c]=y; dsf_c=dsf_c+1;
        y=v1f_1;
        #include "test_cos_store_results.inc"
        // reverse section
        while (csv1_c > save_csv1_c) {
            csv1_c=csv1_c-1;
            if (csv1[csv1_c]==0) {
                dsf_c=dsf_c-1; y=dsf[dsf_c];
                v1f_0=x;
                v1f_1=cos(v1f_0);
                a1_v1f_1=a1_y; a1_y=0;
                a1_v1f_0=-sin(v1f_0)*a1_v1f_1;
                a1_x=a1_x+a1_v1f_0;
            }
        }
        #include "test_cos_restore_results.inc"
    }
    if (a1_mode==2) {
        #include "test_cos_store_inputs.inc"
        a1_mode=a1_mode;
    }
    if (a1_mode==3) {
        #include "test_cos_restore_inputs.inc"
        a1_mode=a1_mode;
    }
}
void a1_test_tan(int a1_mode, double x, double a1_x, double& y, double& a1_y) {
    double v1f_0 = 0;
    double a1_v1f_0 = 0;
    double v1f_1 = 0;
    double a1_v1f_1 = 0;
    int save_csv1_c=0;
    save_csv1_c=csv1_c;
    if (a1_mode==1) {
        // augmented forward section
        csv1[csv1_c]=0; csv1_c=csv1_c+1;
        v1f_0=x;
        v1f_1=tan(v1f_0);
        dsf[dsf_c]=y; dsf_c=dsf_c+1;
        y=v1f_1;
        #include "test_tan_store_results.inc"
        // reverse section
        while (csv1_c > save_csv1_c) {
            csv1_c=csv1_c-1;
            if (csv1[csv1_c]==0) {
                dsf_c=dsf_c-1; y=dsf[dsf_c];
                v1f_0=x;
                v1f_1=tan(v1f_0);
                a1_v1f_1=a1_y; a1_y=0;
                a1_v1f_0=1/(cos(v1f_0)*cos(v1f_0))*a1_v1f_1;
                a1_x=a1_x+a1_v1f_0;
            }
        }
        #include "test_tan_restore_results.inc"
    }
    if (a1_mode==2) {
        #include "test_tan_store_inputs.inc"
        a1_mode=a1_mode;
    }
    if (a1_mode==3) {
        #include "test_tan_restore_inputs.inc"
        a1_mode=a1_mode;
    }
}
void a1_test_atan(int a1_mode, double x, double a1_x, double& y, double& a1_y) {
    double v1f_0 = 0;
    double a1_v1f_0 = 0;
    double v1f_1 = 0;
    double a1_v1f_1 = 0;
    int save_csv1_c=0;
    save_csv1_c=csv1_c;
    if (a1_mode==1) {
        // augmented forward section
        csv1[csv1_c]=0; csv1_c=csv1_c+1;
        v1f_0=x;
        v1f_1=atan(v1f_0);
        dsf[dsf_c]=y; dsf_c=dsf_c+1;
        y=v1f_1;
        #include "test_atan_store_results.inc"
        // reverse section
        while (csv1_c > save_csv1_c) {
            csv1_c=csv1_c-1;
            if (csv1[csv1_c]==0) {
                dsf_c=dsf_c-1; y=dsf[dsf_c];
                v1f_0=x;
                v1f_1=atan(v1f_0);
                a1_v1f_1=a1_y; a1_y=0;
                a1_v1f_0=1/(1+v1f_0*v1f_0)*a1_v1f_1;
                a1_x=a1_x+a1_v1f_0;
            }
        }
        #include "test_atan_restore_results.inc"
    }
    if (a1_mode==2) {
        #include "test_atan_store_inputs.inc"
        a1_mode=a1_mode;
    }
    if (a1_mode==3) {
        #include "test_atan_restore_inputs.inc"
        a1_mode=a1_mode;
    }
}
void a1_test_exp(int a1_mode, double x, double a1_x, double& y, double& a1_y) {
    double v1f_0 = 0;
    double a1_v1f_0 = 0;
    double v1f_1 = 0;
    double a1_v1f_1 = 0;
    int save_csv1_c=0;
    save_csv1_c=csv1_c;
    if (a1_mode==1) {
        // augmented forward section
        csv1[csv1_c]=0; csv1_c=csv1_c+1;
        v1f_0=x;
        v1f_1=exp(v1f_0);
        dsf[dsf_c]=y; dsf_c=dsf_c+1;
        y=v1f_1;
        #include "test_exp_store_results.inc"
        // reverse section
        while (csv1_c > save_csv1_c) {
            csv1_c=csv1_c-1;
            if (csv1[csv1_c]==0) {
                dsf_c=dsf_c-1; y=dsf[dsf_c];
                v1f_0=x;
                v1f_1=exp(v1f_0);
                a1_v1f_1=a1_y; a1_y=0;
                a1_v1f_0=v1f_1*a1_v1f_1;
                a1_x=a1_x+a1_v1f_0;
            }
        }
        #include "test_exp_restore_results.inc"
    }
    if (a1_mode==2) {
        #include "test_exp_store_inputs.inc"
        a1_mode=a1_mode;
    }
    if (a1_mode==3) {
        #include "test_exp_restore_inputs.inc"
        a1_mode=a1_mode;
    }
}
void a1_test_log(int a1_mode, double x, double a1_x, double& y, double& a1_y) {
    double v1f_0 = 0;
    double a1_v1f_0 = 0;
    double v1f_1 = 0;
    double a1_v1f_1 = 0;
    int save_csv1_c=0;
    save_csv1_c=csv1_c;
    if (a1_mode==1) {
        // augmented forward section
        csv1[csv1_c]=0; csv1_c=csv1_c+1;
        v1f_0=x;
        v1f_1=log(v1f_0);
        dsf[dsf_c]=y; dsf_c=dsf_c+1;
        y=v1f_1;
        #include "test_log_store_results.inc"
        // reverse section
        while (csv1_c > save_csv1_c) {
            csv1_c=csv1_c-1;
            if (csv1[csv1_c]==0) {
                dsf_c=dsf_c-1; y=dsf[dsf_c];
                v1f_0=x;
                v1f_1=log(v1f_0);
                a1_v1f_1=a1_y; a1_y=0;
                a1_v1f_0=a1_v1f_1/v1f_0;
                a1_x=a1_x+a1_v1f_0;
            }
        }
        #include "test_log_restore_results.inc"
    }
    if (a1_mode==2) {
        #include "test_log_store_inputs.inc"
        a1_mode=a1_mode;
    }
    if (a1_mode==3) {
        #include "test_log_restore_inputs.inc"
        a1_mode=a1_mode;
    }
}
void a1_test_sqrt(int a1_mode, double x, double a1_x, double& y, double& a1_y) {
    double v1f_0 = 0;
    double a1_v1f_0 = 0;
    double v1f_1 = 0;
    double a1_v1f_1 = 0;
    int save_csv1_c=0;
    save_csv1_c=csv1_c;
    if (a1_mode==1) {
        // augmented forward section
        csv1[csv1_c]=0; csv1_c=csv1_c+1;
        v1f_0=x;
        v1f_1=sqrt(v1f_0);
        dsf[dsf_c]=y; dsf_c=dsf_c+1;
        y=v1f_1;
        #include "test_sqrt_store_results.inc"
        // reverse section
        while (csv1_c > save_csv1_c) {
            csv1_c=csv1_c-1;
            if (csv1[csv1_c]==0) {
                dsf_c=dsf_c-1; y=dsf[dsf_c];
                v1f_0=x;
                v1f_1=sqrt(v1f_0);
                a1_v1f_1=a1_y; a1_y=0;
                a1_v1f_0=1/(2*sqrt(v1f_0))*a1_v1f_1;
                a1_x=a1_x+a1_v1f_0;
            }
        }
        #include "test_sqrt_restore_results.inc"
    }
    if (a1_mode==2) {
        #include "test_sqrt_store_inputs.inc"
        a1_mode=a1_mode;
    }
    if (a1_mode==3) {
        #include "test_sqrt_restore_inputs.inc"
        a1_mode=a1_mode;
    }
}
void a1_test_pow(int a1_mode, double a, double a1_a, double b, double a1_b, double& y, double& a1_y) {
    double v1f_0 = 0;
    double a1_v1f_0 = 0;
    double v1f_1 = 0;
    double a1_v1f_1 = 0;
    double v1f_2 = 0;
    double a1_v1f_2 = 0;
    int save_csv1_c=0;
    save_csv1_c=csv1_c;
    if (a1_mode==1) {
        // augmented forward section
        csv1[csv1_c]=0; csv1_c=csv1_c+1;
        v1f_0=a;
        v1f_1=b;
        v1f_2=pow(v1f_0,v1f_1);
        dsf[dsf_c]=y; dsf_c=dsf_c+1;
        y=v1f_2;
        #include "test_pow_store_results.inc"
        // reverse section
        while (csv1_c > save_csv1_c) {
            csv1_c=csv1_c-1;
            if (csv1[csv1_c]==0) {
                dsf_c=dsf_c-1; y=dsf[dsf_c];
                v1f_0=a;
                v1f_1=b;
                v1f_2=pow(v1f_0,v1f_1);
                a1_v1f_2=a1_y; a1_y=0;
                a1_v1f_0=v1f_1*pow(v1f_0,v1f_1-1)*a1_v1f_2;
                a1_v1f_1=a1_v1f_1+v1f_2*log(v1f_0)*a1_v1f_2;
                a1_b=a1_b+a1_v1f_1;
                a1_a=a1_a+a1_v1f_0;
            }
        }
        #include "test_pow_restore_results.inc"
    }
    if (a1_mode==2) {
        #include "test_pow_store_inputs.inc"
        a1_mode=a1_mode;
    }
    if (a1_mode==3) {
        #include "test_pow_restore_inputs.inc"
        a1_mode=a1_mode;
    }
}
void a1_test_add(int a1_mode, double a, double a1_a, double b, double a1_b, double& y, double& a1_y) {
    double v1f_0 = 0;
    double a1_v1f_0 = 0;
    double v1f_1 = 0;
    double a1_v1f_1 = 0;
    double v1f_2 = 0;
    double a1_v1f_2 = 0;
    int save_csv1_c=0;
    save_csv1_c=csv1_c;
    if (a1_mode==1) {
        // augmented forward section
        csv1[csv1_c]=0; csv1_c=csv1_c+1;
        v1f_0=a;
        v1f_1=b;
        v1f_2=v1f_0+v1f_1;
        dsf[dsf_c]=y; dsf_c=dsf_c+1;
        y=v1f_2;
        #include "test_add_store_results.inc"
        // reverse section
        while (csv1_c > save_csv1_c) {
            csv1_c=csv1_c-1;
            if (csv1[csv1_c]==0) {
                dsf_c=dsf_c-1; y=dsf[dsf_c];
                v1f_0=a;
                v1f_1=b;
                v1f_2=v1f_0+v1f_1;
                a1_v1f_2=a1_y; a1_y=0;
                a1_v1f_0=a1_v1f_2; a1_v1f_1=a1_v1f_2;
                a1_b=a1_b+a1_v1f_1;
                a1_a=a1_a+a1_v1f_0;
            }
        }
        #include "test_add_restore_results.inc"
    }
    if (a1_mode==2) {
        #include "test_add_store_inputs.inc"
        a1_mode=a1_mode;
    }
    if (a1_mode==3) {
        #include "test_add_restore_inputs.inc"
        a1_mode=a1_mode;
    }
}
void a1_test_sub(int a1_mode, double a, double a1_a, double b, double a1_b, double& y, double& a1_y) {
    double v1f_0 = 0;
    double a1_v1f_0 = 0;
    double v1f_1 = 0;
    double a1_v1f_1 = 0;
    double v1f_2 = 0;
    double a1_v1f_2 = 0;
    int save_csv1_c=0;
    save_csv1_c=csv1_c;
    if (a1_mode==1) {
        // augmented forward section
        csv1[csv1_c]=0; csv1_c=csv1_c+1;
        v1f_0=a;
        v1f_1=b;
        v1f_2=v1f_0-v1f_1;
        dsf[dsf_c]=y; dsf_c=dsf_c+1;
        y=v1f_2;
        #include "test_sub_store_results.inc"
        // reverse section
        while (csv1_c > save_csv1_c) {
            csv1_c=csv1_c-1;
            if (csv1[csv1_c]==0) {
                dsf_c=dsf_c-1; y=dsf[dsf_c];
                v1f_0=a;
                v1f_1=b;
                v1f_2=v1f_0-v1f_1;
                a1_v1f_2=a1_y; a1_y=0;
                a1_v1f_0=a1_v1f_2; a1_v1f_1=-a1_v1f_2;
                a1_b=a1_b+a1_v1f_1;
                a1_a=a1_a+a1_v1f_0;
            }
        }
        #include "test_sub_restore_results.inc"
    }
    if (a1_mode==2) {
        #include "test_sub_store_inputs.inc"
        a1_mode=a1_mode;
    }
    if (a1_mode==3) {
        #include "test_sub_restore_inputs.inc"
        a1_mode=a1_mode;
    }
}
void a1_test_mult(int a1_mode, double a, double a1_a, double b, double a1_b, double& y, double& a1_y) {
    double v1f_0 = 0;
    double a1_v1f_0 = 0;
    double v1f_1 = 0;
    double a1_v1f_1 = 0;
    double v1f_2 = 0;
    double a1_v1f_2 = 0;
    int save_csv1_c=0;
    save_csv1_c=csv1_c;
    if (a1_mode==1) {
        // augmented forward section
        csv1[csv1_c]=0; csv1_c=csv1_c+1;
        v1f_0=a;
        v1f_1=b;
        v1f_2=v1f_0*v1f_1;
        dsf[dsf_c]=y; dsf_c=dsf_c+1;
        y=v1f_2;
        #include "test_mult_store_results.inc"
        // reverse section
        while (csv1_c > save_csv1_c) {
            csv1_c=csv1_c-1;
            if (csv1[csv1_c]==0) {
                dsf_c=dsf_c-1; y=dsf[dsf_c];
                v1f_0=a;
                v1f_1=b;
                v1f_2=v1f_0*v1f_1;
                a1_v1f_2=a1_y; a1_y=0;
                a1_v1f_0=a1_v1f_2*v1f_1; a1_v1f_1=v1f_0*a1_v1f_2;
                a1_b=a1_b+a1_v1f_1;
                a1_a=a1_a+a1_v1f_0;
            }
        }
        #include "test_mult_restore_results.inc"
    }
    if (a1_mode==2) {
        #include "test_mult_store_inputs.inc"
        a1_mode=a1_mode;
    }
    if (a1_mode==3) {
        #include "test_mult_restore_inputs.inc"
        a1_mode=a1_mode;
    }
}
void a1_test_div(int a1_mode, double a, double a1_a, double b, double a1_b, double& y, double& a1_y) {
    double v1f_0 = 0;
    double a1_v1f_0 = 0;
    double v1f_1 = 0;
    double a1_v1f_1 = 0;
    double v1f_2 = 0;
    double a1_v1f_2 = 0;
    int save_csv1_c=0;
    save_csv1_c=csv1_c;
    if (a1_mode==1) {
        // augmented forward section
        csv1[csv1_c]=0; csv1_c=csv1_c+1;
        v1f_0=a;
        v1f_1=b;
        v1f_2=v1f_0/v1f_1;
        dsf[dsf_c]=y; dsf_c=dsf_c+1;
        y=v1f_2;
        #include "test_div_store_results.inc"
        // reverse section
        while (csv1_c > save_csv1_c) {
            csv1_c=csv1_c-1;
            if (csv1[csv1_c]==0) {
                dsf_c=dsf_c-1; y=dsf[dsf_c];
                v1f_0=a;
                v1f_1=b;
                v1f_2=v1f_0/v1f_1;
                a1_v1f_2=a1_y; a1_y=0;
                a1_v1f_0=a1_v1f_2/v1f_1; a1_v1f_1=-v1f_0*a1_v1f_2/(v1f_1*v1f_1);
                a1_b=a1_b+a1_v1f_1;
                a1_a=a1_a+a1_v1f_0;
            }
        }
        #include "test_div_restore_results.inc"
    }
    if (a1_mode==2) {
        #include "test_div_store_inputs.inc"
        a1_mode=a1_mode;
    }
    if (a1_mode==3) {
        #include "test_div_restore_inputs.inc"
        a1_mode=a1_mode;
    }
}
