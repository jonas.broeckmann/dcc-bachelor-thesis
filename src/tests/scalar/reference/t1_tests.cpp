void t1_test_const0(double& y, double& t1_y) {
    double v1f_0 = 0;
    double t1_v1f_0 = 0;
    t1_v1f_0=0;
    v1f_0=0;
    t1_y=t1_v1f_0;
    y=v1f_0;
}
void t1_test_const1(double& y, double& t1_y) {
    double v1f_0 = 0;
    double t1_v1f_0 = 0;
    t1_v1f_0=0;
    v1f_0=42;
    t1_y=t1_v1f_0;
    y=v1f_0;
}
void t1_test_assign(double x, double t1_x, double& y, double& t1_y) {
    double v1f_0 = 0;
    double t1_v1f_0 = 0;
    t1_v1f_0=t1_x;
    v1f_0=x;
    t1_y=t1_v1f_0;
    y=v1f_0;
}
void t1_test_paren(double x, double t1_x, double& y, double& t1_y) {
    double v1f_0 = 0;
    double t1_v1f_0 = 0;
    double v1f_1 = 0;
    double t1_v1f_1 = 0;
    t1_v1f_0=t1_x;
    v1f_0=x;
    t1_v1f_1=t1_v1f_0;
    v1f_1=v1f_0;
    t1_y=t1_v1f_1;
    y=v1f_1;
}
void t1_test_uplus(double x, double t1_x, double& y, double& t1_y) {
    double v1f_0 = 0;
    double t1_v1f_0 = 0;
    double v1f_1 = 0;
    double t1_v1f_1 = 0;
    t1_v1f_0=t1_x;
    v1f_0=x;
    t1_v1f_1=t1_v1f_0;
    v1f_1=v1f_0;
    t1_y=t1_v1f_1;
    y=v1f_1;
}
void t1_test_uminus(double x, double t1_x, double& y, double& t1_y) {
    double v1f_0 = 0;
    double t1_v1f_0 = 0;
    double v1f_1 = 0;
    double t1_v1f_1 = 0;
    t1_v1f_0=t1_x;
    v1f_0=x;
    t1_v1f_1=-t1_v1f_0;
    v1f_1=-v1f_0;
    t1_y=t1_v1f_1;
    y=v1f_1;
}
void t1_test_sin(double x, double t1_x, double& y, double& t1_y) {
    double v1f_0 = 0;
    double t1_v1f_0 = 0;
    double v1f_1 = 0;
    double t1_v1f_1 = 0;
    t1_v1f_0=t1_x;
    v1f_0=x;
    t1_v1f_1=cos(v1f_0)*t1_v1f_0;
    v1f_1=sin(v1f_0);
    t1_y=t1_v1f_1;
    y=v1f_1;
}
void t1_test_cos(double x, double t1_x, double& y, double& t1_y) {
    double v1f_0 = 0;
    double t1_v1f_0 = 0;
    double v1f_1 = 0;
    double t1_v1f_1 = 0;
    t1_v1f_0=t1_x;
    v1f_0=x;
    t1_v1f_1=-sin(v1f_0)*t1_v1f_0;
    v1f_1=cos(v1f_0);
    t1_y=t1_v1f_1;
    y=v1f_1;
}
void t1_test_tan(double x, double t1_x, double& y, double& t1_y) {
    double v1f_0 = 0;
    double t1_v1f_0 = 0;
    double v1f_1 = 0;
    double t1_v1f_1 = 0;
    t1_v1f_0=t1_x;
    v1f_0=x;
    t1_v1f_1=1/(cos(v1f_0)*cos(v1f_0))*t1_v1f_0;
    v1f_1=tan(v1f_0);
    t1_y=t1_v1f_1;
    y=v1f_1;
}
void t1_test_atan(double x, double t1_x, double& y, double& t1_y) {
    double v1f_0 = 0;
    double t1_v1f_0 = 0;
    double v1f_1 = 0;
    double t1_v1f_1 = 0;
    t1_v1f_0=t1_x;
    v1f_0=x;
    t1_v1f_1=1/(1+v1f_0*v1f_0)*t1_v1f_0;
    v1f_1=atan(v1f_0);
    t1_y=t1_v1f_1;
    y=v1f_1;
}
void t1_test_exp(double x, double t1_x, double& y, double& t1_y) {
    double v1f_0 = 0;
    double t1_v1f_0 = 0;
    double v1f_1 = 0;
    double t1_v1f_1 = 0;
    t1_v1f_0=t1_x;
    v1f_0=x;
    v1f_1=exp(v1f_0);
    t1_v1f_1=v1f_1*t1_v1f_0;
    t1_y=t1_v1f_1;
    y=v1f_1;
}
void t1_test_log(double x, double t1_x, double& y, double& t1_y) {
    double v1f_0 = 0;
    double t1_v1f_0 = 0;
    double v1f_1 = 0;
    double t1_v1f_1 = 0;
    t1_v1f_0=t1_x;
    v1f_0=x;
    t1_v1f_1=t1_v1f_0/v1f_0;
    v1f_1=log(v1f_0);
    t1_y=t1_v1f_1;
    y=v1f_1;
}
void t1_test_sqrt(double x, double t1_x, double& y, double& t1_y) {
    double v1f_0 = 0;
    double t1_v1f_0 = 0;
    double v1f_1 = 0;
    double t1_v1f_1 = 0;
    t1_v1f_0=t1_x;
    v1f_0=x;
    t1_v1f_1=1/(2*sqrt(v1f_0))*t1_v1f_0;
    v1f_1=sqrt(v1f_0);
    t1_y=t1_v1f_1;
    y=v1f_1;
}
void t1_test_pow(double a, double t1_a, double b, double t1_b, double& y, double& t1_y) {
    double v1f_0 = 0;
    double t1_v1f_0 = 0;
    double v1f_1 = 0;
    double t1_v1f_1 = 0;
    double v1f_2 = 0;
    double t1_v1f_2 = 0;
    t1_v1f_0=t1_a;
    v1f_0=a;
    t1_v1f_1=t1_b;
    v1f_1=b;
    v1f_2=pow(v1f_0,v1f_1);
    t1_v1f_2=v1f_1*pow(v1f_0,v1f_1-1)*t1_v1f_0+v1f_2*log(v1f_0)*t1_v1f_1;
    t1_y=t1_v1f_2;
    y=v1f_2;
}
void t1_test_add(double a, double t1_a, double b, double t1_b, double& y, double& t1_y) {
    double v1f_0 = 0;
    double t1_v1f_0 = 0;
    double v1f_1 = 0;
    double t1_v1f_1 = 0;
    double v1f_2 = 0;
    double t1_v1f_2 = 0;
    t1_v1f_0=t1_a;
    v1f_0=a;
    t1_v1f_1=t1_b;
    v1f_1=b;
    t1_v1f_2=t1_v1f_0+t1_v1f_1;
    v1f_2=v1f_0+v1f_1;
    t1_y=t1_v1f_2;
    y=v1f_2;
}
void t1_test_sub(double a, double t1_a, double b, double t1_b, double& y, double& t1_y) {
    double v1f_0 = 0;
    double t1_v1f_0 = 0;
    double v1f_1 = 0;
    double t1_v1f_1 = 0;
    double v1f_2 = 0;
    double t1_v1f_2 = 0;
    t1_v1f_0=t1_a;
    v1f_0=a;
    t1_v1f_1=t1_b;
    v1f_1=b;
    t1_v1f_2=t1_v1f_0-t1_v1f_1;
    v1f_2=v1f_0-v1f_1;
    t1_y=t1_v1f_2;
    y=v1f_2;
}
void t1_test_mult(double a, double t1_a, double b, double t1_b, double& y, double& t1_y) {
    double v1f_0 = 0;
    double t1_v1f_0 = 0;
    double v1f_1 = 0;
    double t1_v1f_1 = 0;
    double v1f_2 = 0;
    double t1_v1f_2 = 0;
    t1_v1f_0=t1_a;
    v1f_0=a;
    t1_v1f_1=t1_b;
    v1f_1=b;
    t1_v1f_2=t1_v1f_0*v1f_1+v1f_0*t1_v1f_1;
    v1f_2=v1f_0*v1f_1;
    t1_y=t1_v1f_2;
    y=v1f_2;
}
void t1_test_div(double a, double t1_a, double b, double t1_b, double& y, double& t1_y) {
    double v1f_0 = 0;
    double t1_v1f_0 = 0;
    double v1f_1 = 0;
    double t1_v1f_1 = 0;
    double v1f_2 = 0;
    double t1_v1f_2 = 0;
    t1_v1f_0=t1_a;
    v1f_0=a;
    t1_v1f_1=t1_b;
    v1f_1=b;
    t1_v1f_2=t1_v1f_0/v1f_1-v1f_0*t1_v1f_1/(v1f_1*v1f_1);
    v1f_2=v1f_0/v1f_1;
    t1_y=t1_v1f_2;
    y=v1f_2;
}
