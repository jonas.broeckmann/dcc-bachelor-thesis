
void test_const0(double& y) {
    y = 0;
}

void test_const1(double& y) {
    y = 42;
}

void test_assign(double x, double& y) {
    y = x;
}

void test_paren(double x, double& y) {
    y = (x);
}

void test_uplus(double x, double& y) {
    y = +x;
}

void test_uminus(double x, double& y) {
    y = -x;
}

void test_sin(double x, double& y) {
    y = sin(x);
}

void test_cos(double x, double& y) {
    y = cos(x);
}

void test_tan(double x, double& y) {
    y = tan(x);
}

void test_atan(double x, double& y) {
    y = atan(x);
}

void test_exp(double x, double& y) {
    y = exp(x);
}

void test_log(double x, double& y) {
    y = log(x);
}

void test_sqrt(double x, double& y) {
    y = sqrt(x);
}

void test_pow(double a, double b, double& y) {
    y = pow(a, b);
}

void test_add(double a, double b, double& y) {
    y = a + b;
}

void test_sub(double a, double b, double& y) {
    y = a - b;
}

void test_mult(double a, double b, double& y) {
    y = a * b;
}

void test_div(double a, double b, double& y) {
    y = a / b;
}
