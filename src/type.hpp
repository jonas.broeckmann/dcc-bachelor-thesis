#ifndef TYPE
#define TYPE

#include <fstream>
#include <assert.h>

using namespace std;

struct type_data {

  enum struct kinds {
    void_t = 0,
    bool_t,
    float_t,
    int_t
  };

  using dimension = int;

  enum struct decomp_kinds {
    none = 0,
    partial_piv_lu,
    full_piv_lu,
    householder_qr,
    col_piv_householder_qr,
    full_piv_householder_qr,
    complete_orthogonal_decomposition,
    llt,
    ldlt
  };

  using access_kind = int;


  kinds kind;

  /// Rows of the type: "dimensions::scalar" if scalar, 1 or greater if vector, "dimensions::eigen_dynamic" for dynamic vector
  dimension rows;
  /// Columns of the type: "dimensions::scalar" if scalar, 1 or greater if vector, "dimensions::eigen_dynamic" for dynamic vector
  dimension cols;

  decomp_kinds decomp_kind;

  /// value, reference or pointer
  access_kind access;


  type_data() : type_data(kinds::void_t, dimensions::scalar, dimensions::scalar) {}
  type_data(kinds kind, dimension rows, dimension cols) : kind(kind), rows(rows), cols(cols), access(access_kinds::value), decomp_kind(decomp_kinds::none) {
    assert(kind != kinds::void_t || (rows == dimensions::none && cols == dimensions::none));
    assert(kind != kinds::bool_t || (rows == dimensions::none && cols == dimensions::none));
  }
  
  struct dimensions {
    static const dimension none = 0;
    static const dimension scalar = 0;
    static const dimension eigen_dynamic = -1;
    static bool match(dimension a, dimension b) {
      return a == b || (a == eigen_dynamic && is_vector(b)) || (b == eigen_dynamic && is_vector(a));
    };
    static bool is_vector(dimension d) {
      return d >= 1 || d == eigen_dynamic;
    };
    static bool is_fixed(dimension d) {
      return d != eigen_dynamic;
    };
  };

  struct access_kinds {
    static const access_kind value = 0;
    static const access_kind reference = -1;
    static bool is_pointer(access_kind k) {
      return k >= 1;
    };
  };

  bool is_scalar(void) const {
    return rows == dimensions::scalar && cols == dimensions::scalar && is_algebraic();
  }
  bool is_vector(void) const {
    return dimensions::is_vector(rows) && dimensions::is_vector(cols) && is_algebraic();
  }
  bool is_algebraic(void) const {
    return (kind == kinds::float_t || kind == kinds::int_t) && !is_decomposition();
  }
  bool is_decomposition(void) const {
    return decomp_kind != decomp_kinds::none;
  }
  bool is_active(void) const {
    return kind == kinds::float_t;
  }
  bool is_fixed_size(void) const {
    return dimensions::is_fixed(rows) && dimensions::is_fixed(cols);
  }

  /// unparse the type (only the base part)
  void unparse_val(ostream&) const;
  /// unparse the type
  void unparse(ostream&) const;

  bool matches(const type_data& other) const {
    if (!(kind == other.kind || (is_scalar() && other.is_scalar()))) {
      return false; // kinds don't match
    }
    if (!(dimensions::match(rows, other.rows) && dimensions::match(cols, other.cols))) {
      return false;
    }
    if (!(decomp_kind == other.decomp_kind)) {
      return false;
    }
    if (access_kinds::is_pointer(access) || access_kinds::is_pointer(other.access)) {
      return false;
    }
    return true;
  }

  void unique_id(ostream& stream) const {
    switch (kind) {
      case type_data::kinds::void_t : stream << "v"; break;
      case type_data::kinds::bool_t : stream << "b"; break;
      case type_data::kinds::float_t: stream << "f"; break;
      case type_data::kinds::int_t  : stream << "i"; break;
      default: assert(false);
    }
    if (is_vector() || is_decomposition()) {
      stream << rows << cols;
    }
    if (is_decomposition()) {
      stream << "d" << static_cast<int>(decomp_kind);
    }
  }

  type_data implicit_conversion_to_active(void) const {
    if (is_scalar() && kind == kinds::int_t) {
      type_data copy = *this;
      copy.kind = kinds::float_t;
      return copy;
    }
    return *this;
  }

  static const type_data void_type;
  static const type_data bool_type;
  static const type_data float_scalar;
  static const type_data int_scalar;
  static const type_data float_mat(dimension rows, dimension cols) {
    assert(dimensions::is_vector(rows));
    assert(dimensions::is_vector(cols));
    return type_data(kinds::float_t, rows, cols);
  };

  static decomp_kinds decomposition_from_eigen_function(string function) {
    if (function == "lu"                             ) return decomp_kinds::partial_piv_lu;
    if (function == "partialPivLu"                   ) return decomp_kinds::partial_piv_lu;
    if (function == "fullPivLu"                      ) return decomp_kinds::full_piv_lu;
    if (function == "householderQr"                  ) return decomp_kinds::householder_qr;
    if (function == "colPivHouseholderQr"            ) return decomp_kinds::col_piv_householder_qr;
    if (function == "fullPivHouseholderQr"           ) return decomp_kinds::full_piv_householder_qr;
    if (function == "completeOrthogonalDecomposition") return decomp_kinds::complete_orthogonal_decomposition;
    if (function == "llt"                            ) return decomp_kinds::llt;
    if (function == "ldlt"                           ) return decomp_kinds::ldlt;
    return decomp_kinds::none;
  }

  static decomp_kinds decomposition_from_eigen_type(string type) {
    if (type == "PartialPivLU"                   ) return decomp_kinds::partial_piv_lu;
    if (type == "FullPivLU"                      ) return decomp_kinds::full_piv_lu;
    if (type == "HouseholderQR"                  ) return decomp_kinds::householder_qr;
    if (type == "ColPivHouseholderQR"            ) return decomp_kinds::col_piv_householder_qr;
    if (type == "FullPivHouseholderQR"           ) return decomp_kinds::full_piv_householder_qr;
    if (type == "CompleteOrthogonalDecomposition") return decomp_kinds::complete_orthogonal_decomposition;
    if (type == "LLT"                            ) return decomp_kinds::llt;
    if (type == "LDLT"                           ) return decomp_kinds::ldlt;
    return decomp_kinds::none;
  }

  static string decomposition_to_eigen_function(decomp_kinds decomposition) {
    switch (decomposition) {
      case decomp_kinds::partial_piv_lu                   : return "partialPivLu";
      case decomp_kinds::full_piv_lu                      : return "fullPivLu";
      case decomp_kinds::householder_qr                   : return "householderQr";
      case decomp_kinds::col_piv_householder_qr           : return "colPivHouseholderQr";
      case decomp_kinds::full_piv_householder_qr          : return "fullPivHouseholderQr";
      case decomp_kinds::complete_orthogonal_decomposition: return "completeOrthogonalDecomposition";
      case decomp_kinds::llt                              : return "llt";
      case decomp_kinds::ldlt                             : return "ldlt";
      default: assert(false);
    }
  }

  static string decomposition_to_eigen_type(decomp_kinds decomposition) {
    switch (decomposition) {
      case decomp_kinds::partial_piv_lu                   : return "PartialPivLU";
      case decomp_kinds::full_piv_lu                      : return "FullPivLU";
      case decomp_kinds::householder_qr                   : return "HouseholderQR";
      case decomp_kinds::col_piv_householder_qr           : return "ColPivHouseholderQR";
      case decomp_kinds::full_piv_householder_qr          : return "FullPivHouseholderQR";
      case decomp_kinds::complete_orthogonal_decomposition: return "CompleteOrthogonalDecomposition";
      case decomp_kinds::llt                              : return "LLT";
      case decomp_kinds::ldlt                             : return "LDLT";
      default: assert(false);
    }
  }

};

#endif