#ifndef DCC
#define DCC

#include <string>
#include "ast.hpp"

using namespace std;

extern ast the_ast;

extern string sac_var_prefix;
extern string tl_var_prefix;
extern string adj_var_prefix;

extern int css;
extern int dss;

extern string amode_var_name;

// flags to configure how the code is generated

#define OPT_DECOMPOSITION_IN_SAC    true
#define OPT_FS_SAC                  true
#define OPT_FS_SAC_STORE            true
#define OPT_GENERATE_INCLUDES       true

#if OPT_FS_SAC_STORE
#undef OPT_FS_SAC
#define OPT_FS_SAC true
#endif

#endif