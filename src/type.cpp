#include <assert.h>
#include <iostream>
#include "type.hpp"
#include "parser.hpp"

const type_data type_data::void_type = type_data(kinds::void_t, 0, 0);
const type_data type_data::bool_type = type_data(kinds::bool_t, 0, 0);
const type_data type_data::float_scalar = type_data(kinds::float_t, 0, 0);
const type_data type_data::int_scalar = type_data(kinds::int_t, 0, 0);

void type_data::unparse_val(ostream& outstream) const {
  if (is_decomposition()) {
    outstream << decomposition_to_eigen_type(decomp_kind) << "<";
    type_data child = *this;
    child.decomp_kind = decomp_kinds::none;
    child.unparse_val(outstream);
    outstream << ">";
    return;
  }
  switch (kind) {
    case kinds::void_t: {
      if (rows == dimensions::none && cols == dimensions::none) {
        outstream << "void";
      } else {
        cerr << "ERROR: Type 'void' can't have rows or columns" << endl;
        assert(false);
      }
      break;
    }
    case kinds::float_t: {
      if (is_scalar()) {
        outstream << "double";
      } else {
        // special cases for shorter matrix types
        if (rows == dimensions::eigen_dynamic && cols == dimensions::eigen_dynamic) {
          outstream << "Matrix" << "X" << "d";
        } else if (rows == dimensions::eigen_dynamic && (1 <= cols && cols <= 4)) {
          if (cols == 1) outstream << "Vector" << "X";
          else outstream << "Matrix" << "X" << cols;
          outstream << "d";
        } else if (cols == dimensions::eigen_dynamic && (1 <= rows && rows <= 4)) {
          if (rows == 1) outstream << "RowVector" << "X";
          else outstream << "Matrix" << rows << "X";
          outstream << "d";
        } else if (rows == cols && (2 <= rows && rows <= 4)) {
          outstream << "Matrix" << rows << "d";
        } else if (cols == 1 && (2 <= rows && rows <= 4)) {
          outstream << "Vector" << rows << "d";
        } else if (rows == 1 && (2 <= cols && cols <= 4)) {
          outstream << "RowVector" << cols << "d";
        } else { // everything else that has no typedef
          outstream << "Matrix<double,";
          if (rows >= 1) outstream << rows;
          else if (rows == dimensions::eigen_dynamic) outstream << "Dynamic";
          else {
            cerr << "ERROR: Unknown row dimension '" << rows << "'" << endl;
            assert(false);
          }
          outstream << ",";
          if (cols >= 1) outstream << cols;
          else if (cols == dimensions::eigen_dynamic) outstream << "Dynamic";
          else {
            cerr << "ERROR: Unknown column dimension '" << cols << "'" << endl;
            assert(false);
          }
          outstream << ">";
        }
      }
      break;
    }
    case kinds::int_t: {
      if (is_scalar()) {
        outstream << "int";
      } else {
        cerr << "ERROR: Type 'int' can only be scalar" << endl;
        assert(false);
      }
      break;
    }
    default : {
      cerr << "ERROR: Unknown type kind '" << static_cast<int>(kind) << "'" << endl;
      assert(false);
      break;
    }
  }
}

void type_data::unparse(ostream& outstream) const {
  unparse_val(outstream);
  if (access == access_kinds::reference) {
    if (kind == kinds::void_t) {
      cerr << "ERROR: Type 'void' can't be a reference" << endl;
      assert(false);
    }
    outstream << "& ";
  } else if (access_kinds::is_pointer(access)) {
    outstream << " ";
    for (int i = 0; i < access; i++) outstream << "*";
  } else {
    outstream << " ";
  }
}

