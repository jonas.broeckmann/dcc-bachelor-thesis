#include <iostream>
#include <cstdlib>
#include <sstream>
#include <assert.h>
#include "dcc.hpp"
#include "tangent_linear_code.hpp"
#include "ast.hpp"
#include "util.hpp"


void tangent_linear_code::build(ostream& outstream) {
  // unparse tangent-linear code
  if (the_ast.ast_root) unparse(the_ast.ast_root,outstream,0);
}

static type_data tl_type(type_data type) {
  type.decomp_kind = type_data::decomp_kinds::none;
  return type;
}

static symbol tl_symbol(symbol *sym) {
  symbol tl_sym;
  tl_sym.name = tl_var_prefix + sym->name;
  tl_sym.kind = sym->kind;
  tl_sym.type = tl_type(sym->type);
  tl_sym.dim = sym->dim;
  if (sym->dim != 0) {
    tl_sym.val = sym->val;
    // cerr << "Tangents on arrays aren't supported" << endl;
    // assert(false);
  } else {
    tl_sym.val = string("0");
  }
  return tl_sym;
}

static string sac_var(expr_ast_vertex *expr) {
  return expr->sac_var->name;
}

static string tl_sac_var(expr_ast_vertex *expr) {
  return tl_var_prefix + expr->sac_var->name;
}

void tangent_linear_code::unparse(ast_vertex* v, ostream& outstream, int indent) {
  if (!v) return;
  list<ast_vertex*>::const_iterator it;
  switch (v->type) {
    case CODE_ASTV: {
      it=v->children.begin();
      unparse(*it,outstream,indent);
      unparse(*(++it),outstream,indent);
      break;
    }
    case SEQUENCE_OF_SUBROUTINES_ASTV: {
      for (it=v->children.begin();it!=v->children.end();it++) 
        unparse((*it),outstream,indent);
      break;
    }
    case SUBROUTINE_ASTV: {
      it=v->children.begin();
      tl_symbol(static_cast<symbol_ast_vertex*>(*it)->sym).unparse_decl(outstream);
      outstream << "(";
      unparse(*(++it),outstream,indent);
      outstream << ") {" << endl;
      indent++;
      unparse(*(++it),outstream,indent);
      unparse(*(++it),outstream,indent);
      indent--;
      outstream << "}" << endl;
      break;
    }
    case SUBROUTINE_ARG_LIST_ASTV: {
      for (it=v->children.begin();it!=v->children.end();it++) {
        symbol_ast_vertex *astv = static_cast<symbol_ast_vertex*>(*it);
        astv->sym->unparse_decl(outstream);
        if (astv->sym->type.is_active()) {
          outstream << ", ";
          tl_symbol(astv->sym).unparse_decl(outstream);
        }
        if (astv != v->children.back()) outstream << ", ";
      }
      break;
    }
    case SEQUENCE_OF_DECLARATIONS_ASTV: {
      for (it=v->children.begin();it!=v->children.end();it++) {
        symbol_ast_vertex *astv = static_cast<symbol_ast_vertex*>(*it);
        indent(outstream);
        astv->sym->unparse_decl(outstream);
        outstream << ";" << endl;
        if (astv->sym->type.is_active()) {
          indent(outstream);
          tl_symbol(astv->sym).unparse_decl(outstream);
          outstream << ";" << endl;
        }
      }
      break;
    }
    case SEQUENCE_OF_STATEMENTS_ASTV: {
      for (it=v->children.begin();it!=v->children.end();it++) 
        unparse((*it),outstream,indent);
      break;
    }
    case IF_STATEMENT_ASTV: {
      it=v->children.begin();
      indent(outstream); outstream << "if (";
      (*it)->unparse(outstream,indent);
      outstream << ") {" << endl;
      indent++;
      unparse((*(++it)),outstream,indent);
      indent--;
      indent(outstream); outstream << "}";
      if (v->children.size()==3) {
        outstream << " else {" << endl;
        indent++;
        unparse(*(++it),outstream,indent);
        indent--;
        indent(outstream); outstream << "}" << endl;
      }
      outstream << endl;
      break;
    }
    case WHILE_STATEMENT_ASTV: {
      it=v->children.begin();
      indent(outstream); outstream << "while (";
      (*it)->unparse(outstream,indent);
      outstream << ") {" << endl;
      indent++;
      unparse((*(++it)),outstream,indent);
      indent--;
      indent(outstream); outstream << "}" << endl;
      break;
    }
    case ASSIGNMENT_ASTV: {
      assignment_ast_vertex *astv = static_cast<assignment_ast_vertex*>(v);
      if (astv->lhs->sym->type.is_active()) {
          unparse(astv->rhs,outstream,indent);

          symbol tl_sym = tl_symbol(astv->lhs->sym);
          memref_ast_vertex *tl_lhs;
          switch (astv->lhs->type) {
            case SCALAR_MEMREF_ASTV:
              tl_lhs = new memref_ast_vertex(&tl_sym);
              break;
            case ARRAY_MEMREF_ASTV:
              tl_lhs = new array_memref_ast_vertex(&tl_sym);
              static_cast<array_memref_ast_vertex*>(tl_lhs)->offset = static_cast<array_memref_ast_vertex*>(astv->lhs)->offset;
              break;
            default: assert(false); break;
          }

          indent(outstream);
          tl_lhs->unparse(outstream, 0);
          outstream << "=" << tl_sac_var(astv->rhs) << ";" << endl;
          indent(outstream);
          astv->lhs->unparse(outstream, 0);
          outstream << "=" << sac_var(astv->rhs) << ";" << endl;

          delete tl_lhs;
      } else {
          astv->unparse(outstream, indent);
      }  
      break; 
    }
    case SUBROUTINE_CALL_ASTV: {
      indent(outstream);
      tl_symbol(static_cast<symbol_ast_vertex*>(child(v, 0))->sym).unparse_access(outstream);
      outstream << "(";
      unparse(child(v, 1), outstream, indent);
      outstream << ");" << endl;
      break;
    }
    case SUBROUTINE_CALL_ARG_LIST_ASTV: {
      for (it = v->children.begin(); it != v->children.end(); it++) {
        symbol_ast_vertex *astv = static_cast<symbol_ast_vertex*>(*it);
        astv->unparse(outstream,indent);
        if (astv->sym->type.is_active()) {
          outstream << ", ";
          tl_symbol(astv->sym).unparse_access(outstream);
        }
        if (astv != v->children.back()) outstream << ", ";
      }
      break;
    }
    // y=x1+x2
    case ADD_EXPRESSION_ASTV: {
      binary_expr_ast_vertex *astv = static_cast<binary_expr_ast_vertex*>(v);
      unparse(astv->lhs,outstream,indent);
      unparse(astv->rhs,outstream,indent);
      // tangent-linear code
      indent(outstream);
      outstream << tl_sac_var(astv) // d_y 
                << "="
                << tl_sac_var(astv->lhs) // d_x1
                << "+" 
                << tl_sac_var(astv->rhs) // d_x2
                << ";" << endl; 
      indent(outstream);
      outstream << sac_var(astv) // y 
                << "=" 
                << sac_var(astv->lhs) // x1
                << "+" 
                << sac_var(astv->rhs) // x2
                << ";" << endl; 
      break;
    }
    // y=x1-x2
    case SUB_EXPRESSION_ASTV: {
      binary_expr_ast_vertex *astv = static_cast<binary_expr_ast_vertex*>(v);
      unparse(astv->lhs,outstream,indent);
      unparse(astv->rhs,outstream,indent);
      // tangent-linear code
      indent(outstream);
      outstream << tl_sac_var(astv) // d_y 
                << "=" 
                << tl_sac_var(astv->lhs) // d_x1
                << "-" 
                << tl_sac_var(astv->rhs) // d_x2
                << ";" << endl; 
      indent(outstream);
      outstream << sac_var(astv) // y 
                << "=" 
                << sac_var(astv->lhs) // x1
                << "-" 
                << sac_var(astv->rhs) // x2
                << ";" << endl; 
      break;
    }
    // y=x1*x2
    case MUL_EXPRESSION_ASTV: {
      binary_expr_ast_vertex *astv = static_cast<binary_expr_ast_vertex*>(v);
      unparse(astv->lhs,outstream,indent);
      unparse(astv->rhs,outstream,indent);
      // tangent-linear code
      indent(outstream);
      outstream << tl_sac_var(astv) // d_y 
                << "=" 
                << tl_sac_var(astv->lhs) // d_x1
                << "*"
                << sac_var(astv->rhs) // x2
                << "+" 
                << sac_var(astv->lhs) // x1
                << "*" 
                << tl_sac_var(astv->rhs) // d_x2
                << ";" << endl; 
      indent(outstream);
      outstream << sac_var(astv) // y 
                << "=" 
                << sac_var(astv->lhs) // x1
                << "*" 
                << sac_var(astv->rhs) // x2
                << ";" << endl; 
      break;
    }
    // y=x1/x2
    case DIV_EXPRESSION_ASTV: {
      binary_expr_ast_vertex *astv = static_cast<binary_expr_ast_vertex*>(v);
      unparse(astv->lhs,outstream,indent);
      unparse(astv->rhs,outstream,indent);
      // tangent-linear code
      indent(outstream);
      outstream << tl_sac_var(astv) // d_y 
                << "=" 
                << tl_sac_var(astv->lhs) // d_x1
                << "/" 
                << sac_var(astv->rhs) // x2
                << "-" 
                << sac_var(astv->lhs) // x1
                << "*" 
                << tl_sac_var(astv->rhs) // d_x2
                << "/(" 
                << sac_var(astv->rhs) // x2
                << "*" 
                << sac_var(astv->rhs) // x2
                << ")" 
                << ";" << endl; 
      indent(outstream);
      outstream << sac_var(astv) // y 
                << "=" 
                << sac_var(astv->lhs) // x1
                << "/" 
                << sac_var(astv->rhs) // x2
                << ";" << endl; 
      break;
    }
    // y=(x);
    case PAR_EXPRESSION_ASTV: {
      unary_expr_ast_vertex *astv = static_cast<unary_expr_ast_vertex*>(v);
      unparse(astv->child,outstream,indent);
      // tangent-linear code
      indent(outstream);
      outstream << tl_sac_var(astv) // d_y 
                << "=" 
                << tl_sac_var(astv->child) // d_x
                << ";" << endl; 
      indent(outstream);
      outstream << sac_var(astv) // y 
                << "=" 
                << sac_var(astv->child) // x
                << ";" << endl; 
      break;
    }
    // y=+x;
    case UPLUS_EXPRESSION_ASTV: {
      unary_expr_ast_vertex *astv = static_cast<unary_expr_ast_vertex*>(v);
      unparse(astv->child,outstream,indent);
      // tangent-linear code
      indent(outstream);
      outstream << tl_sac_var(astv) // d_y 
                << "="
                << tl_sac_var(astv->child) // d_x
                << ";" << endl; 
      indent(outstream);
      outstream << sac_var(astv) // y 
                << "="
                << sac_var(astv->child) // x
                << ";" << endl; 
      break;
    }
    // y=-x;
    case UMINUS_EXPRESSION_ASTV: {
      unary_expr_ast_vertex *astv = static_cast<unary_expr_ast_vertex*>(v);
      unparse(astv->child,outstream,indent);
      // tangent-linear code
      indent(outstream);
      outstream << tl_sac_var(astv) // d_y 
                << "="
                << "-"
                << tl_sac_var(astv->child) // d_x
                << ";" << endl; 
      indent(outstream);
      outstream << sac_var(astv) // y 
                << "="
                << "-"
                << sac_var(astv->child) // x
                << ";" << endl; 
      break;
    }
    // y=sin(x);
    case SIN_EXPRESSION_ASTV: {
      unary_expr_ast_vertex *astv = static_cast<unary_expr_ast_vertex*>(v);
      unparse(astv->child,outstream,indent);
      // tangent-linear code
      indent(outstream);
      outstream << tl_sac_var(astv) // d_y 
                << "=" 
                << "cos(" 
                << sac_var(astv->child) // x
                << ")*" 
                << tl_sac_var(astv->child) // d_x
                << ";" << endl; 
      indent(outstream);
      outstream << sac_var(astv) // y 
                << "=sin(" 
                << sac_var(astv->child) // x
                << ");" << endl; 
      break;
    }
    // y=cos(x);
    case COS_EXPRESSION_ASTV: {
      unary_expr_ast_vertex *astv = static_cast<unary_expr_ast_vertex*>(v);
      unparse(astv->child,outstream,indent);
      // tangent-linear code
      indent(outstream);
      outstream << tl_sac_var(astv) // d_y 
                << "=" 
                << "-sin(" 
                << sac_var(astv->child) // x
                << ")*" 
                << tl_sac_var(astv->child) // d_x
                << ";" << endl; 
      indent(outstream);
      outstream << sac_var(astv) // y 
                << "=cos(" 
                << sac_var(astv->child) // x
                << ");" << endl; 
      break;
    }
    // y=tan(x);
    case TAN_EXPRESSION_ASTV: {
      unary_expr_ast_vertex *astv = static_cast<unary_expr_ast_vertex*>(v);
      unparse(astv->child,outstream,indent);
      // tangent-linear code
      indent(outstream);
      outstream << tl_sac_var(astv) // d_y
                << "=1/("
                << "cos("
                << sac_var(astv->child) // x
                << ")*"
                << "cos("
                << sac_var(astv->child) // x
                << "))*"
                << tl_sac_var(astv->child) // d_x
                << ";" << endl;
      indent(outstream);
      outstream << sac_var(astv) // y
                << "=tan("
                << sac_var(astv->child) // x
                << ");" << endl;
      break;
    }

    // y=exp(x);
    case EXP_EXPRESSION_ASTV: {
      unary_expr_ast_vertex *astv = static_cast<unary_expr_ast_vertex*>(v);
      unparse(astv->child,outstream,indent);
      // tangent-linear code
      indent(outstream);
      outstream << sac_var(astv) // y 
                << "=exp(" 
                << sac_var(astv->child) // x
                << ");" << endl; 
      indent(outstream);
      outstream << tl_sac_var(astv) // d_y 
                << "=" 
                << sac_var(astv) // y
                << "*" 
                << tl_sac_var(astv->child) // d_x
                << ";" << endl; 
      break;
    }
    // y=sqrt(x);
    case SQRT_EXPRESSION_ASTV: {
      unary_expr_ast_vertex *astv = static_cast<unary_expr_ast_vertex*>(v);
      unparse(astv->child,outstream,indent);
      // tangent-linear code
      indent(outstream);
      outstream << tl_sac_var(astv) // d_y 
                << "=" 
                << "1/(2*sqrt(" 
                << sac_var(astv->child) // x
                << "))*" 
                << tl_sac_var(astv->child) // d_x
                << ";" << endl; 
      indent(outstream);
      outstream << sac_var(astv) // y 
                << "=sqrt(" 
                << sac_var(astv->child) // x
                << ");" << endl; 
      break;
    }
    // y=atan(x);
    case ATAN_EXPRESSION_ASTV: {
      unary_expr_ast_vertex *astv = static_cast<unary_expr_ast_vertex*>(v);
      unparse(astv->child,outstream,indent);
      // tangent-linear code
      indent(outstream);
      outstream << tl_sac_var(astv) // d_y 
                << "=" 
                << "1/(1+" 
                << sac_var(astv->child) // x
                << "*" 
                << sac_var(astv->child) // x
                << ")*" 
                << tl_sac_var(astv->child) // d_x
                << ";" << endl; 
      indent(outstream);
      outstream << sac_var(astv) // y 
                << "=atan(" 
                << sac_var(astv->child) // x
                << ");" << endl; 
      break;
    }
    // y=log(x);
    case LOG_EXPRESSION_ASTV: {
      unary_expr_ast_vertex *astv = static_cast<unary_expr_ast_vertex*>(v);
      unparse(astv->child,outstream,indent);
      // tangent-linear code
      indent(outstream);
      outstream << tl_sac_var(astv) // d_y
                << "="
                << tl_sac_var(astv->child) // d_x
                << "/"
                << sac_var(astv->child) // x
                << ";" << endl;
      indent(outstream);
      outstream << sac_var(astv) // y
                << "=log("
                << sac_var(astv->child) // x
                << ");" << endl;
      break;  
    }
    // y=pow(x,z)
    case POW_EXPRESSION_ASTV: {
      binary_expr_ast_vertex *astv = static_cast<binary_expr_ast_vertex*>(v);
      unparse(astv->lhs,outstream,indent);
      unparse(astv->rhs,outstream,indent);
      // tangent-linear code
      indent(outstream); // y=pow(x,z)
      outstream << sac_var(astv) // y
                << "=pow("
                << sac_var(astv->lhs) // x
                << ","
                << sac_var(astv->rhs) // z
                << ");" << endl;
      indent(outstream); // d_y=z*pow(x,z-1)*d_x+y*log(x)*d_z;
      outstream << tl_sac_var(astv) // d_y
                << "="
                << sac_var(astv->rhs) // z
                << "*pow("
                << sac_var(astv->lhs) // x
                << ","
                << sac_var(astv->rhs) // z
                << "-1)*"
                << tl_sac_var(astv->lhs); // d_x
      if (astv->rhs->get_res_type().is_active()) {
        outstream << "+"
                  << sac_var(astv) // y
                  << "*log("
                  << sac_var(astv->lhs) // x
                  << ")*"
                  << tl_sac_var(astv->rhs); // d_z
      }
      outstream << ";" << endl;
      break;
    }
    // y=x.eval();
    case EVAL_EXPRESSION_ASTV: {
      unary_expr_ast_vertex *astv = static_cast<unary_expr_ast_vertex*>(v);
      unparse(astv->child,outstream,indent);
      // tangent-linear code
      indent(outstream);
      outstream << tl_sac_var(astv) // d_y 
                << "="
                << tl_sac_var(astv->child) // d_x
                << ";" << endl;
      indent(outstream);
      outstream << sac_var(astv) // y 
                << "="
                << sac_var(astv->child) // x
                << ";" << endl;
      break;
    }
    // y=x.transpose();
    case TRANSPOSE_EXPRESSION_ASTV: {
      unary_expr_ast_vertex *astv = static_cast<unary_expr_ast_vertex*>(v);
      if (astv->child->get_res_type().is_decomposition()) assert(false);
      unparse(astv->child,outstream,indent);
      // tangent-linear code
      indent(outstream);
      outstream << tl_sac_var(astv) // d_y 
                << "="
                << tl_sac_var(astv->child) // d_x
                << ".transpose()"
                << ";" << endl;
      indent(outstream);
      outstream << sac_var(astv) // y 
                << "="
                << sac_var(astv->child) // x
                << ".transpose()"
                << ";" << endl;
      break;
    }
    // y=x.inverse();
    case INVERSE_EXPRESSION_ASTV: {
      unary_expr_ast_vertex *astv = static_cast<unary_expr_ast_vertex*>(v);
      unparse(astv->child,outstream,indent);
      // tangent-linear code
      indent(outstream);
      outstream << sac_var(astv) // y         // compute before d_y to only calculate the inverse once
                << "="
                << sac_var(astv->child) // x
                << ".inverse()"
                << ";" << endl;
      indent(outstream);
      outstream << tl_sac_var(astv) // d_y 
                << "="
                << "-"
                << sac_var(astv) // x^-1
                << "*"
                << tl_sac_var(astv->child) // d_x
                << "*"
                << sac_var(astv) // x^-1
                << ";" << endl;
      break;
    }
    // y=x.trace();
    case TRACE_EXPRESSION_ASTV: {
      unary_expr_ast_vertex *astv = static_cast<unary_expr_ast_vertex*>(v);
      unparse(astv->child,outstream,indent);
      // tangent-linear code
      indent(outstream);
      outstream << tl_sac_var(astv) // d_y 
                << "="
                << tl_sac_var(astv->child) // d_x
                << ".trace()"
                << ";" << endl;
      indent(outstream);
      outstream << sac_var(astv) // y 
                << "="
                << sac_var(astv->child) // x
                << ".trace()"
                << ";" << endl;
      break;
    }
    // y=x.determinant();
    case DETERMINANT_EXPRESSION_ASTV: {
      unary_expr_ast_vertex *astv = static_cast<unary_expr_ast_vertex*>(v);
      unparse(astv->child,outstream,indent);
      // tangent-linear code
      outstream << sac_var(astv) // y 
                << "="
                << sac_var(astv->child) // x
                << ".determinant()"
                << ";" << endl;
      indent(outstream);
      outstream << tl_sac_var(astv) // d_y 
                << "="
                << sac_var(astv) // y
                << "*("
                << sac_var(astv->child) // x
                << ".inverse()"
                << "*"
                << tl_sac_var(astv->child) // d_x
                << ").trace()"
                << ";" << endl;
      indent(outstream);
      break;
    }
    // y=x1.dot(x2)
    case DOT_EXPRESSION_ASTV: {
      binary_expr_ast_vertex *astv = static_cast<binary_expr_ast_vertex*>(v);
      unparse(astv->lhs,outstream,indent);
      unparse(astv->rhs,outstream,indent);
      // tangent-linear code
      indent(outstream);
      outstream << tl_sac_var(astv) // d_y 
                << "=" 
                << tl_sac_var(astv->lhs) // d_x1
                << ".dot("
                << sac_var(astv->rhs) // x2
                << ")"
                << "+" 
                << sac_var(astv->lhs) // x1
                << ".dot("
                << tl_sac_var(astv->rhs) // d_x2
                << ")"
                << ";" << endl; 
      indent(outstream);
      outstream << sac_var(astv) // y 
                << "=" 
                << sac_var(astv->lhs) // x1
                << ".dot("
                << sac_var(astv->rhs) // x2
                << ")"
                << ";" << endl; 
      break;
    }
    // x=A.decomp().solve(b) or
    // x="SAC".solve(b)
    case SOLVE_EXPRESSION_ASTV: {
      binary_expr_ast_vertex *astv = static_cast<binary_expr_ast_vertex*>(v);

      expr_ast_vertex *decomp_source;
      string decomp_var;
      if (OPT_DECOMPOSITION_IN_SAC) {
        unparse(astv->lhs,outstream,indent);
        decomp_source = astv->lhs;
        decomp_var = sac_var(astv->lhs);
      } else {
        unary_expr_ast_vertex *orig_decomp = static_cast<unary_expr_ast_vertex*>(astv->lhs);
        unparse(orig_decomp->child,outstream,indent);
        string decomp_fun = type_data::decomposition_to_eigen_function(orig_decomp->get_res_type().decomp_kind);
        decomp_source = orig_decomp->child;
        decomp_var = sac_var(orig_decomp->child) + "." + decomp_fun + "()";
      }
      unparse(astv->rhs,outstream,indent);
      // tangent-linear code
      indent(outstream);
      outstream << sac_var(astv) // x
                << "=" 
                << decomp_var
                << ".solve("
                << sac_var(astv->rhs) // b
                << ")"
                << ";" << endl;
      indent(outstream);
      outstream << tl_sac_var(astv) // d_x
                << "="
                << decomp_var
                << ".solve("
                << "-"
                << tl_sac_var(decomp_source) // d_A
                << "*"
                << sac_var(astv) // x
                << ")"
                << "+"
                << decomp_var
                << ".solve("
                << tl_sac_var(astv->rhs) // d_b
                << ")"
                << ";" << endl;
      break;
    }
    // x=A.decomp().transpose().solve(b) or
    // x="SAC".transpose().solve(b)
    case SOLVE_TRANSPOSED_EXPRESSION_ASTV: {
      binary_expr_ast_vertex *astv = static_cast<binary_expr_ast_vertex*>(v);

      expr_ast_vertex *decomp_source;
      string decomp_var_transposed;
      if (OPT_DECOMPOSITION_IN_SAC) {
        unparse(astv->lhs,outstream,indent);
        decomp_source = astv->lhs;
        decomp_var_transposed = sac_var(astv->lhs) + ".transpose()";
      } else {
        unary_expr_ast_vertex *orig_decomp = static_cast<unary_expr_ast_vertex*>(astv->lhs);
        unparse(orig_decomp->child,outstream,indent);
        string decomp_fun = type_data::decomposition_to_eigen_function(orig_decomp->get_res_type().decomp_kind);
        decomp_source = orig_decomp->child;
        decomp_var_transposed = sac_var(orig_decomp->child) + "." + decomp_fun + "()" + ".transpose()";
      }
      unparse(astv->rhs,outstream,indent);
      // tangent-linear code
      indent(outstream);
      outstream << sac_var(astv) // x
                << "=" 
                << decomp_var_transposed
                << ".solve("
                << sac_var(astv->rhs) // b
                << ")"
                << ";" << endl;
      indent(outstream);
      outstream << tl_sac_var(astv) // d_x
                << "="
                << decomp_var_transposed
                << ".solve("
                << "-"
                << tl_sac_var(decomp_source) // d_A
                << "*"
                << sac_var(astv) // x
                << ").eval()"
                << "+"
                << decomp_var_transposed
                << ".solve("
                << tl_sac_var(astv->rhs) // d_b
                << ").eval()"
                << ";" << endl;
      break;
    }
    // B=A.decomp()
    case DECOMP_EXPRESSION_ASTV: {
      if (!OPT_DECOMPOSITION_IN_SAC) assert(false);
      unary_expr_ast_vertex *astv = static_cast<unary_expr_ast_vertex*>(v);
      unparse(astv->child,outstream,indent);
      // tangent-linear code
      indent(outstream);
      outstream << tl_sac_var(astv) // d_B
                << "="
                << tl_sac_var(astv->child) // d_A
                << ";" << endl;
      indent(outstream);
      outstream << sac_var(astv) // B
                << "="
                << sac_var(astv->child) // A
                << "."
                << type_data::decomposition_to_eigen_function(astv->get_res_type().decomp_kind)
                << "()"
                << ";" << endl;
      break;
    }
    // y=x;
    case SCALAR_MEMREF_ASTV: {
      memref_ast_vertex *astv = static_cast<memref_ast_vertex*>(v);
      // tangent-linear code
      indent(outstream);
      outstream << tl_sac_var(astv) // d_y 
                << "="; 
      if (astv->sym->type.is_active()) {
        symbol tl_sym = tl_symbol(astv->sym);
        memref_ast_vertex tl_astv(&tl_sym);
        tl_astv.unparse(outstream, 0); // d_x
      } else {
        symbol::constant(tl_type(astv->get_res_type()), "0").unparse_access(outstream);
      }
      outstream << ";" << endl;
      indent(outstream);
      outstream << sac_var(astv) // y 
                << "=";
      astv->unparse(outstream, 0); // x
      outstream << ";" << endl; 
      break;
    }
    // y=x[i];
    case ARRAY_MEMREF_ASTV: {
      array_memref_ast_vertex *astv = static_cast<array_memref_ast_vertex*>(v);

      // tangent-linear code
      indent(outstream);
      outstream << tl_sac_var(astv) // d_y
              << "=";
      if (astv->sym->type.is_active()) {
        symbol tl_sym = tl_symbol(astv->sym);
        array_memref_ast_vertex tl_astv(&tl_sym);
        tl_astv.offset = astv->offset;
        tl_astv.unparse(outstream, 0); // d_x
      } else {
        symbol::constant(tl_type(astv->get_res_type()), "0").unparse_access(outstream);
      }
      outstream << ";" << endl; 
      indent(outstream);
      outstream << sac_var(astv) // y
                << "=";
      astv->unparse(outstream, 0); // x
      outstream << ";" << endl; 
      break;
    }
    case CONSTANT_ASTV: {
      const_ast_vertex *astv = static_cast<const_ast_vertex*>(v);
      type_data type = tl_type(astv->get_res_type());
      // tangent-linear code
      indent(outstream);
      outstream << tl_sac_var(astv) // d_y 
                << "=";
      symbol::constant(type, "0").unparse_access(outstream);
      outstream << ";" << endl; 
      indent(outstream);
      outstream << sac_var(astv) // y 
                << "=";
      astv->sym->unparse_access(outstream);
      outstream << ";" << endl; 
      break;
    }
    default: {
      cerr << "Unknown ast vertex type " << v->type << endl;
      exit(-2);
      break;
    }
  }
}


